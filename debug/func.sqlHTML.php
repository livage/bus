<?php
function sqlHTML($pmConnection, $pmSql, $pmDebug = 0) {

	$returnData = sqlExecute($pmConnection, $pmSql, $sqlError, $count, $pmDebug);

	$result = $returnData["result"];
	$model = $returnData["model"];

	$retHTML = "<table class'sql_data'><tr class='header'>";

	$c=0;
	foreach ($model as $fieldname => $attributes) {
		$retHTML .= "<th>$fieldname</th>";
		$c++;
	}

	if (count($result)) {
		for($r=0; $r<count($result); $r++) {
			$retHTML .= "<tr class='row type".($r%2)."'>";
			foreach ($result[$r] as $fieldname => $value) {
				$retHTML .= "<td>$value</td>";
			};
			$retHTML .= "</tr>";
		}
	} else
		$retHTML .= "<tr><td colspan='$c'>Nessun risultato presente</td></tr>";

	$retHTML .= "</table>";
}
?>