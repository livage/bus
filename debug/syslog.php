<?php
require_once("inc.init.debug.php");
require_once("../core/func.mysqlPrepare.php");
require_once("../core/func.mysqlDate.php");
require_once("../core/func.mysqlDateTime.php");

if ($_POST["nuovaricerca"]) {

	$_SESSION[SITE_NAME]["TREARTfilter"] = array(
		"file" 		=> $_POST["file"],
		"utente" 	=> $_POST["utente"],
		"comando" 	=> $_POST["comando"],
		"timestamp_da"	=> $_POST["timestamp_da"],
		"timestamp_a"	=> $_POST["timestamp_a"],
		"type_command"	=> $_POST["type_command"],
		"error"		=> $_POST["error"],
		"message"	=> $_POST["message"],
		"perpagina" 	=> $_POST["perpagina"],
	);
	$page = 1;

} else{
	$page = intval($_GET["pg"])?intval($_GET["pg"]):1;
	$_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"] = 50;
	$_SESSION[SITE_NAME]["TREARTfilter"]["error"] = "";

}
if($_SESSION[SITE_NAME]["TREARTfilter"]["file"]){
	$arrValues = explode(" ", $_SESSION[SITE_NAME]["TREARTfilter"]["file"]);
	    for ($i=0; $i<count($arrValues); $i++)
	        $sqlWhere .= " AND page_master LIKE '%". mysqlPrepare($arrValues[$i])."%' ";
}
if($_SESSION[SITE_NAME]["TREARTfilter"]["message"]){
	$arrValues = explode(" ", $_SESSION[SITE_NAME]["TREARTfilter"]["message"]);
	    for ($i=0; $i<count($arrValues); $i++)
	        $sqlWhere .= " AND message LIKE '%".mysqlPrepare($arrValues[$i])."%' ";
}
if($_SESSION[SITE_NAME]["TREARTfilter"]["utente"]){
	$arrValues = explode(" ", $_SESSION[SITE_NAME]["TREARTfilter"]["utente"]);
		for ($i=0; $i<count($arrValues); $i++)
	        $sqlWhere .= " AND username LIKE '%".mysqlPrepare($arrValues[$i])."%' ";
}
if($_SESSION[SITE_NAME]["TREARTfilter"]["comando"]){
	$arrValues = explode(" ", $_SESSION[SITE_NAME]["TREARTfilter"]["comando"]);
		for ($i=0; $i<count($arrValues); $i++)
	        $sqlWhere .= " AND statement LIKE '%".mysqlPrepare($arrValues[$i])."%' ";
}
if($_SESSION[SITE_NAME]["TREARTfilter"]["type_command"]){
	$sqlWhere .= " AND type_command = '". $_SESSION[SITE_NAME]["TREARTfilter"]["type_command"] ."'";
}
if(strlen($_SESSION[SITE_NAME]["TREARTfilter"]["error"])){
	$sqlWhere .= " AND error = ". intval($_SESSION[SITE_NAME]["TREARTfilter"]["error"]) ;
}
if($_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_da"] && $_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_da"] != "__/__/____"){
	$sqlWhere .= " AND timestamp >= " .mysqlDate($_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_da"]);
}
if($_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_a"] && $_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_a"] != "__/__/____"){
	$sqlWhere .= " AND timestamp <= " .mysqlDate($_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_a"]);
}


$resultPerPage  = $_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"];
$pagelist = 40;
$sql =  "   SELECT count(*) as counter FROM _log WHERE 1 $sqlWhere ";

$data = sqlExecute($connection,$sql,$sqlError,$sqlCount,basename(__FILE__));
$result = intval($data[0]["counter"]);
$pages = ceil($data[0]["counter"]/$resultPerPage);

$pageMinus = floor(($pagelist-1) / 2);
$pagePlus = ceil(($pagelist-1) / 2);

$pagine = range(max(1, $page-$pageMinus), min($pages,$page+max($pagelist-$page, $pagePlus)));

$sql =  "   SELECT logid, page_master, page_detail, username, type_command, error as errore,
				DATE_FORMAT(timestamp, '%d/%m/%Y %H:%i:%S') AS date, statement, message
			FROM _log
			WHERE 1 $sqlWhere
			ORDER BY logid DESC
			LIMIT ".($resultPerPage*($page-1)).", $resultPerPage";
$data = sqlExecute($connection,$sql,$sqlError,$sqlCount,basename(__FILE__));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>SYSLOG <?php echo SITE_NAME?></title>
<link rel="stylesheet" type="text/css" href="syslog.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery.maskedinput.js"></script>
<script type="text/javascript" src="jquery.charcounter.js"></script>
<script type="text/javascript" src="shadowbox.js"></script>
<script>
jQuery(function($){
	$("#time_da").mask("99/99/9999");
	$("#time_a").mask("99/99/9999");
});
Shadowbox.loadSkin('classic', '../libs/shadowbox/skin');

window.onload = function(){

	Shadowbox.init({
		skipSetup: true
	});

};
function vediquery(prmId){
	Shadowbox.open({
	    player:     'iframe',
	    title:      'Query Results',
	    content:    'syssql.php?id='+prmId,
	    height:     650,
	    width:      950
	});
}
function vedidettaglio(prmId){
	Shadowbox.open({
	    player:     'iframe',
	    title:      'LOG Detail',
	    content:    'sysdetail.php?id='+prmId,
	    height:     650,
	    width:      950
	});
}
function openquery(){
	Shadowbox.open({
	    player:     'iframe',
	    title:      'Execute custom query',
	    content:    'query.htm',
	    height:     650,
	    width:      950
	});
}
function truncate_log(){
	alert('funzionalitą da sviluppare');
}
</script>
</head>
<body>
<table>
<tr>
    <td colspan="7" id="paginazione">
<?php
if ($pages > 1){
	print "Results: <strong>". $result ."</strong> - Pagine:";
	if ($pagine[0] != 1)
		print "<a href='syslog.php?pg=1'>1</a> | ...";
	for ($p=$pagine[0]; $p<count($pagine)+$pagine[0]; $p++)
	    if ($p==$page) echo " | <b>$p</b>";
	    else echo " | <a href='syslog.php?pg=$p'>$p</a>";
	if(($p -1)!= $pages)
		print " | ... | <a href='syslog.php?pg=$pages'>$pages</a> |";
} else
	print "Results: <strong>". $result ."</strong>";
?>
    </td>
</tr>
<tr>
	<td colspan="7">
		<button type="button" onclick="openquery()">Esecute Query</button>
		<button type="button" onclick="alert('TODO')">Export XLS</button>
		<button type="button" onclick="alert('TODO')">Export SQL</button>
		<button type="button" onclick="truncate_log()">Truncate LOG</button>
		<button type="button" onclick="alert('TODO')">Export SQL + Truncate LOG</button>
	</td>
</tr>
<tr>
    <th>File</th>
    <th>User</th>
    <th>Timestamp</th>
    <th>Command Type</th>
    <th>Statement</th>
    <th>Error</th>
    <th>Message</th>
</tr>
<form method="POST" action="syslog.php">
<tr>
    <td class="filter">
    	<input type="text" name="file" size="10" maxlength="255" value="<?php echo $_SESSION[SITE_NAME]["TREARTfilter"]["file"]?>">
    </td>
    <td class="filter">
    	<input type="text" name="utente" size="10" maxlength="30" value="<?php echo $_SESSION[SITE_NAME]["TREARTfilter"]["utente"]?>">
    </td>
    <td class="filter">
		From <input type="text" name="timestamp_da" id="time_da" value="<?php echo $_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_da"]?>" class="time"/><br />
		to <input type="text" name="timestamp_a" id="time_a" value="<?php echo $_SESSION[SITE_NAME]["TREARTfilter"]["timestamp_a"]?>" class="time"/>
    </td>
    <td class="filter">
		<select name="type_command">
		<option></option>
		<option value="S" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["type_command"] == "S"?"selected":"") ?>>SQL</option>
		<option value="E" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["type_command"] == "E"?"selected":"") ?>>ENGINE</option>
		</select>
    </td>
    <td class="filter">
    	<input type="text" name="comando" size="35" maxlength="255" value="<?php echo $_SESSION[SITE_NAME]["TREARTfilter"]["comando"]?>">
    </td>
    <td class="filter">
		<select name="error">
		<option></option>
		<option value="1" <?php echo (intval($_SESSION[SITE_NAME]["TREARTfilter"]["error"]) == 1?"selected":"") ?>>Yes</option>
		<option value="0" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["error"] === 0?"selected":"") ?>>No</option>
		</select>
    </td>
    <td class="filter">
    	<input type="text" name="message" size="25" maxlength="255" value="<?php echo $_SESSION[SITE_NAME]["TREARTfilter"]["message"]?>">
    </td>
</tr>
<tr>
	<td colspan="6" class="filter">
		<strong>Results per page</strong>:
		<select name="perpagina">
		<option value="25" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"] == 25?"selected":"") ?>>25</option>
		<option value="50" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"] == 50?"selected":"") ?>>50</option>
		<option value="100" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"] == 100?"selected":"") ?>>100</option>
		<option value="200" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"] == 200?"selected":"") ?>>200</option>
		<option value="500" <?php echo ($_SESSION[SITE_NAME]["TREARTfilter"]["perpagina"] == 500?"selected":"") ?>>500</option>
		</select>
	</td>
	<td class="filter" style="text-align: right;">
    	<input type="hidden" name="nuovaricerca" value="1">
    	<button type="submit">FILTER</button>
    </td>
</tr>
</form>
<?php for ($i=0; $i<count($data); $i++) { ?>
<tr class="over i<?php echo $i%2?>">
    <td><?php echo $data[$i]["page_master"]?><br /><?php echo $data[$i]["page_detail"]?></td>
    <td><?php echo $data[$i]["username"]?></td>
    <td><?php echo $data[$i]["date"]?></td>
    <td><img src="<?php echo $data[$i]["type_command"]?>.png" onclick='vedidettaglio(<?php echo  $data[$i]["logid"] ?>)'/></td>
    <td>
    	<?php
    	if ($data[$i]["type_command"] == "S")
    		print "<a href='javascript:vediquery(". $data[$i]["logid"] .")'>". $data[$i]["statement"]."<a/>";
    	else
    		print $data[$i]["statement"];
    	?>
    </td>
    <td><?php echo ($data[$i]["errore"] == 1?"<img src='../images/icons/delete.gif' />":"") ?></td>
    <td><?php echo $data[$i]["message"]?></td>
</tr>
<?php } ?>
<tr>
    <td colspan="7" id="paginazione">
<?php
if ($pages > 1){
	print "Results: <strong>". $result ."</strong> - Pagine:";
	if ($pagine[0] != 1)
		print "<a href='syslog.php?pg=1'>1</a> | ...";
	for ($p=$pagine[0]; $p<count($pagine)+$pagine[0]; $p++)
	    if ($p==$page) echo " | <b>$p</b>";
	    else echo " | <a href='syslog.php?pg=$p'>$p</a>";
	if(($p -1)!= $pages)
		print " | ... | <a href='syslog.php?pg=$pages'>$pages</a> |";
} else
	print "Results: <strong>". $result ."</strong>";
?>
    </td>
</tr>
</table>
</body>
</html>
