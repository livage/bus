<?php
session_start();
if (stripos(strtoupper(PHP_OS), 'WIN') !== FALSE) { // Windows
	ini_set('include_path', ini_get('include_path').';cfg/;core/;../core/;../cfg/;../');
} else { // *ix
	ini_set('include_path', ini_get('include_path').':cfg/:core/:../core/:../cfg/:../');
}
require("../cfg/sys.db.config.php");
$fileData = pathinfo(__FILE__);
$normalizedPath = str_replace('\\', '/', $fileData[dirname]);
define('LOCAL_ROOT_PATH',
        substr($normalizedPath, 0, strlen($normalizedPath)-strlen("debug")));

// Search first "/" occurrence
$slashPos = strpos(LOCAL_ROOT_PATH, "/");

if (substr($_SERVER['DOCUMENT_ROOT'], strlen($_SERVER['DOCUMENT_ROOT'])-1 ,1) == "/")
	$documentRoot = $_SERVER['DOCUMENT_ROOT'];
else
	$documentRoot = $_SERVER['DOCUMENT_ROOT']."/";
define ('SITE_NAME',
        substr( $normalizedPath,
                strlen($documentRoot),
                strlen($normalizedPath)-strlen("debug")-strlen($documentRoot)-1)
);
if (SITE_NAME == '') {
	define('SITE_NAME', $_SERVER['HTTP_HOST']);
	define('WEB_FOLDER', "/");
} else
	define('WEB_FOLDER', SITE_NAME."/");

// CONFIGURATION
$connection = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD);
mysql_select_db(DB_NAME);
require_once("func.sqlExecute.php");
?>