<?php
require_once('core/func.mysqlPrepare.php');

switch ($_SESSION[SITE_NAME]['login_data']['profile']) {
	case 'A':
		break;
	case 'S':
		break;
	case 'P':
		break;
	case 'T':
	$sql = 'SELECT	*
							
					FROM	_users
						
					WHERE	id='.$_SESSION[SITE_NAME]['login_data']['id'];
			$tvdata = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			
			$_SESSION[SITE_NAME]['login_data']['tv']=$tvdata['tv'];
			$_SESSION[SITE_NAME]['login_data']['tvid']=$tvdata['tvid'];
		break;	
		
	case 'U':
			$sql = 'SELECT	p.*,
							DATE_FORMAT(p.dateOfBirth, \'%d/%m/%Y\') dateOfBirthDMY,
							DATE_FORMAT(p.weddingDate, \'%d/%m/%Y\') weddingDateDMY,
							DATE_FORMAT(p.insTS, \'%d/%m/%Y\') registrtionDateDMY,
							CONCAT(FLOOR(DATEDIFF(CURDATE(), p.dateOfBirth)/365), \'y \', ROUND(MOD(DATEDIFF(CURDATE(), p.dateOfBirth),365)/30), \'m\') age,
							g.list_label genderText,
							m.list_label maritalStatusText,
							c.commonName countryText,
							c.iso3166_1_3letter countryCode,
							wr.worldRegion
					FROM	people p
						JOIN _list_values g ON p.gender = g.list_value AND g.list_name = \'GENDER\'
						JOIN _list_values m ON p.maritalStatus = m.list_value AND m.list_name = \'MARSTA\'
						JOIN countries c ON p.country = c.iso3166_1_3letter
						JOIN worldregions wr ON wr.id = c.region
					WHERE	p.email = \''.mysqlPrepare($_SESSION[SITE_NAME]['login_data']['username']).'\'';
			$peopleData = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);

			if ($peopleData['id']) {
				if ($peopleData['picture']) $peopleData['picture'] = PHOTO_PATH.'USR_'.$peopleData['id']."_0_".$peopleData['picture'];
				$_SESSION[SITE_NAME]['login_data']['person'] = $peopleData;
			} else {
				header('Location: do.logout.php');
			}

		break;
}

?>