<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');


if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}


$fields = array(
	'STRING' => array(
		'name',
		'driver',
		'bus_no',
					
	),
	'INT' => array(
	'bus_seats',
	
	),
	'FLOAT' => array(
	'cost',
	),
	'DATE' => array(
	'departure_date',
	),
	'DATETIME' => array(
	
	),
	
	'TIME' => array(
	'departure_time',
	),
);

$mainTable = 'trip';
$nextPage = 'index.php?c=trip_list';

switch ($op) {
    
    case 'I': // Insert
 
    $id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError);
	 //placing the main route
	 		$sql = 'INSERT INTO routes_trip (ismain, route,trip,price, company, insU,insTS ) VALUES ( 1,'.$_POST['route'].', '.$id.', '.$_POST['price'].', "'.$_SESSION[SITE_NAME]['login_data']['company'].'",   "'.$_SESSION[SITE_NAME]['login_data']['username'].'", NOW())';
echo $sql; 
				$mainroute = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
	// pacing subroutes
	  if($_POST['withS']=='on'){
		  //update has subroutes
	$value = array(
'withSubRoute'=>1,	
);
	    doOperation($connection, 'U', $mainTable, $fields, $value, $sqlError, $id);
	  
	  foreach($_POST['selector'] as $value ){
		  
	 		$sql = 'INSERT INTO routes_trip
            ( route,trip,price, company, insU,insTS )
VALUES (
        '.$_POST['subroute'][$value].',
       '.$id.',
        '.$_POST['cost'][$value].',
        "'.$_SESSION[SITE_NAME]['login_data']['company'].'",
       "'.$_SESSION[SITE_NAME]['login_data']['username'].'",
        NOW())';

				$subtrip = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
	  }
	   
	  }
	
     
        break;
    case "U": // Update
    	
        doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
		 if($_POST['image']=='on'){ 
	 
	  move_uploaded_file($_FILES['team_image']["tmp_name"], "images/logos/".$id."logo.jpg" );
	
	$value = array(
'image'=>1,	
);
	    doOperation($connection, 'U', $mainTable, $fields, $value, $sqlError, $id);
	  }
		echo $sqlError;
        break;
      case 'D'://Status
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
       
   case 'S'://Status
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
    
}
?>
<script language="javascript" type="text/javascript">
    window.location = "<?php echo $nextPage?>";
</script>