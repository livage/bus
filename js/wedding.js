
// JavaScript Document
$(document).ready(function(){

//Municipality auto_complete
$('[id="person"]').autocomplete( // Autocomplete - Search behaviour
 	"ajax.getPerson.php",{
  minChars: 3, // min nbr of char before autocomplete fire
  max: 30, // max nbr of results shown
  delay: 100, // time to wait before autocomplete fire
  autoFill: false, // flag to decide if while the user write the word is completing
  mustMatch: false, // flag to decide if accept only values shown in the list
  matchContains: true, //
  scrollHeight: 300, // hieght of the list showed
  formatItem: function(row, i, max) { // Function to format the values when found
   var retStr;
   if (row.length > 1) {
  retStr = row[1] + "<br />" + row[2] + "<br />" + row[3] + "<br />" + row[4] + "<br />" + row[5]; 
   } else
    retStr = row[0];
   return retStr; // Return HTML code to show foreach row 
  },
  formatResult: function(row) { //Function returning the value to put on the field
   if (row.length > 1)  // e.g. if something found
    //return row[0];  //   first field
     return row[0];  //   first field
	
      
   else
    return '';   //   else blank string
  }
 }
 
 );
$('[id="person"]').result(function(event, data, formatted) { // Autocomplete - Result behaviour

if (data) { // If something selected
	//$('[id]').val(data[2]);
	$('[id=person]').val(data[1]);
	$('[name=surname]').val(data[2]);
	$('[name=city]').val(data[3]);
	$('[name=country]').val(data[4]);
	$('[name=weddingDate]').val(data[5]);
	

 }
});// End of Municipality autocomplete
})





