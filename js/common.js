$(document).ready(function() {
	initMenu();

    $("#searchCriteria").hide(0);
    $("#subscribe").hide(0);
    $(".third_div").hide(0);

    // Form automated validation
    $("form").submit(function() {
        var error = validateForm(this);
        if (error == true) {
            alert("Sono presenti degli errori. Si prega di correggerli e procedere nuovamente.\n");
        }
        return (error==false);
    });
    /************************************************************************/

    // Field activation
    $("[type=text], select, textarea").focus(function(){
        $(this).addClass('_activeControl');

    })
    // Field deactivation
    $("[type=text], select, textarea").blur(function(){
        $(this).removeClass('_activeControl');
    })
	//redirect
$("tr.game ").click(function(){

   window.location = "index.php?c=sale&id="+$(this).attr("id");
 });
})


// JavaScript Document
function reset_search(){
    $("input[type=text], select, input[type=checkbox]").each(function(){
    //$("#searchCriteria1").find("select, input[type=text], input[type=checkbox]").each(function(){
        $(this).val('');
    })
    $("form").submit();
}



/*function showSearchForm(){
    //	$("#searchForm").animate({"height": "toggle"}, { duration: 100 });
    if ($(".third_div").is(":visible")) {
        $(".third_div").slideToggle(300);
    }
    else if ($("#importCriteria").is(":visible")) {
        $("#importCriteria").slideToggle(300);
    }
    $("#searchCriteria").slideToggle(300);
    return false;
}*/

/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */

/*function showExportFormats(){
    //	$("#searchForm").animate({"height": "toggle"}, { duration: 100 });
    if ($("#searchCriteria").is(":visible")) {
        $("#searchCriteria").slideToggle(300);
    }else if ($("#importCriteria").is(":visible")) {
        $("#importCriteria").slideToggle(300);
    }
    $(".third_div").slideToggle(300);
    return false;
}*/

/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function subscribe(){
 // $("#subscribe").animate({"height": "toggle"}, { duration: 100 });
    $("#subscribe").slideToggle(300);
    return false;
}

/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function check_needbank_status(){

    var id = $('[id=payment_type]').val();
    $.post("ajax.check_pt_status.php", {
        id:id
    },
    function(result){

        if(result == 1){
            $('[class=boxfield] [id=paymentBank]').hide();

            $('[class=boxfield] [id=paymentBank]').show();
        }else{
            $('[class=boxfield] [id=paymentBank]').hide();
        }
    });
}



/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function initMenu() {
     $("a", "#menu").removeClass('activeMenu');
     if ($("#_contentClass").val()) {
          $("#"+$("#_contentClass").val()).addClass('activeMenu');
     }
}

/* ************************************************************* */
/* ************************************************************* */
/* ************************************************************* */
/* ************************************************************* */
function remove_attachment(rID, rSection, rType) {
//alert
	if (confirm('Are you sure you want to remove this '+rType+'')){
		$.ajax({
			type: "POST",
			url: "ajax.deleteAttachment.php?id="+rID+'&section='+rSection+'&type='+rType,
			success: function(result){
				if (result!='KO') {
					$("#image_"+rID+"").remove();
				}
			},
			error: function () {
				alert('L\'allegato non pu� essere cancellato');
			}
		});
	}
}
/* ************************************************************* */
$(document).ready(function(){
	 $("tbody tr:odd").css('background-color','#FFFFFF');
	 $("tbody tr:even").css('background-color','#E6E6E6');

 });

 

