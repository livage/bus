var startHeight = {};
$(document).ready(function(){
	$("textarea").css("overflow", "hidden");
	$("textarea").each(function() {
		$(this).keyup(function() {
			var s = parseInt($(this).attr("scrollHeight"));
			var h = parseInt($(this).css("height"));
			var lh = parseInt($(this).css("line-height"));
			if(s > h) {
				$(this).css("height", s);
			}
		})
	})
})

function phraseDelete(pId) {
	if (confirm('Do you confirm the deletion?')) {
		window.location = 'do.phrases.php?op=D&id='+pId;
	}
}