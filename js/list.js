var rowNum = 0;

$(document).ready(function() {
	/* SEARCH BOX */
	$("div.searchCon").each(function(){
		var container = $(this);
		$("a.showdiv", container).click(function(){
			var oldIdx = $(".searchbox", container).index($(".searchbox:visible", container));
			var newIdx = $("a.showdiv", container).index($(this));
			if (oldIdx == newIdx) {
				$("a.showdiv", container).eq(oldIdx).removeClass("active");
				$(".searchbox", container).eq(oldIdx).slideUp(400);
			} else {
				$("a.showdiv", container).eq(oldIdx).removeClass("active");
				$(".searchbox", container).eq(oldIdx).slideUp(400, function(){
					$(".searchbox", container).eq(newIdx).slideDown(400);
					$("a.showdiv", container).eq(newIdx).addClass("active");
				});
			}
		});
	})
	$("div.searchbox").hide();

	/* FORM AUTOMATION */
	$(".boxfield input[type=text], .boxfield input[type=file], .boxfield input[type=password], .boxfield select, .boxfield textarea").focus(function() {
		$(this).addClass('active');
	}).blur(function(){
		$(this).removeClass('active');
	})
})

function get_list_values(){
	var list = $('[name=listame]').val();
	window.location='index.php?c=business_new&list='+list

}//