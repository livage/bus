function newFriend() {
	$("#friends").append('<div class="friend"></div>');
	$.ajax({
		url: "htmls/box/o_box_person.htm",
		success: function(result){
			var myFriend = $(".friend", "#friends").eq($(".friend", "#friends").length-1);
			myFriend.hide();
        	myFriend.html(result);
        	$(".personTitle span", myFriend).html($(".friend", "#friends").length);
        	myFriend.slideDown(400);
        	$(".removeFriend", myFriend).click(function(){
				$(myFriend).css("background-color", "red");
				if (confirm('Are you sure to not register this friend?')) {
					$(myFriend).slideUp(400,function() {
						$(myFriend).remove();
					})
				} else {
					$(myFriend).css("background-color", "");
				}
        	})

      	}
	});
}

$(document).ready(function(){
	$(".removeFriend").click(function(){
		var divToRemove = $(this).parents(".friend");
		$(divToRemove).css("background-color", "red");
		if (confirm('Are you sure to not register this friend?')) {
			$(divToRemove).fadeOut(400,function() {
				$(divToRemove).remove();
			})
		} else {
			$(divToRemove).css("background-color", "");
		}
	})
	$(".friend .personTitle span", "#friends").html("1");
})