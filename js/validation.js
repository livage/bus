var	_fTmpDate = new	Date();

//	Version	1.1
function validateForm(pObject)	{
	var	error = false;
	var	errorStr = "";
	var	checks;
	$("[_fChecks]").removeClass("_fError");
	$("div._mError").remove();
	var	fieldValue;

//	var	validationNo = 0;
	var	isCustomized = false;

	$("[_fChecks]", pObject).each(function()	{
		fieldValue = '';
		switch($(this).attr('type'))	{
			case 'checkbox':
			case 'radio':
					if	($(this).attr("id"))	{
						fieldValue = ($("#"+$(this).attr("id")+":checked").val())
					} else if	($(this).attr("name"))	{
						fieldValue = ($("[name="+$(this).attr("name")+"]:checked").val())
					}
				break;
			default:
					fieldValue = $(this).val();
				break;
		}

		checks = $(this).attr("_fChecks").split("	");
		for	(i=0;	i<checks.length;	i++)	{
			switch(checks[i])	{
				case 'mandatory':
						if	(fieldValue == '')	{
							setError(this, 'Obbligatorio');
							error = true;
						}
					break;
				case 'email':
						if	(!emailCheck(fieldValue))	{
							setError(this, 'E-Mail errata');
							error = true;
						}
					break;
			}
		}
	})

	switch	(error)	{
		case true:
				return 1;
			break;
		case false:
				return 0;
			break;
	}
}

function setError(pField, pMessage)	{
	$(pField).parents('.fieldbox').append('<div	class="_mError">'+pMessage+'</div>');
	$(pField).addClass('_fError');
}

function emailCheck(str)	{

	var	at="@"
	var	dot="."
	var	lat=str.indexOf(at)
	var	lstr=str.length
	var	ldot=str.indexOf(dot)
	if	(str.indexOf(at)==-1){
//				alert("Invalid	E-mail	ID")
				return false
	}

	if	(str.indexOf(at)==-1	||	str.indexOf(at)==0	||	str.indexOf(at)==lstr){
//				alert("Invalid	E-mail	ID")
				return false
	}

	if	(str.indexOf(dot)==-1	||	str.indexOf(dot)==0	||	str.indexOf(dot)==lstr){
//					alert("Invalid	E-mail	ID")
					return false
	}

		if	(str.indexOf(at,(lat+1))!=-1){
//					alert("Invalid	E-mail	ID")
					return false
		}

		if	(str.substring(lat-1,lat)==dot	||	str.substring(lat+1,lat+2)==dot){
//					alert("Invalid	E-mail	ID")
					return false
		}

		if	(str.indexOf(dot,(lat+2))==-1){
//					alert("Invalid	E-mail	ID")
					return false
		}

		if	(str.indexOf("	")!=-1){
//					alert("Invalid	E-mail	ID")
					return false
		}

		return true
}
