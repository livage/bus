/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function addAttrValue(pWhere, pWhich, pWhat) {

    if ($(pWhere).attr(pWhich) != undefined) {
        if ($(pWhere).attr(pWhich).indexOf(pWhat) == -1) {
            $(pWhere).attr(pWhich, $(pWhere).attr(pWhich)+' '+pWhat);
        }
    }
}

/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function removeAttrValue(pWhere, pWhich, pWhat) {
    if ($(pWhere).attr(pWhich) != undefined) {
        if ($(pWhere).attr(pWhich).indexOf(pWhat) != -1) {
            $(pWhere).attr(pWhich, $(pWhere).attr(pWhich).replace(pWhat, ''));
        }
    }
}

/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function serialize(pObject) {
	var retString = "";
	var	controlName;
	$("[type=text], [type=radio], [type=checkbox], [type=password], [type=hidden], select, textarea", $(pObject)).each(function() {
		controlName = $(this).attr("name");
		if (controlName != undefined && controlName != '') {
			retString += (controlName+"="+$(this).val()+"&");
		}
	})
	return retString;
}
/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function ajaxSubmit(pObject, pUrl, pFunction) {

	if (pFunction != undefined && pFunction != '') {
		eval(pFunction+"("+($(pObject).attr("id")?"'#"+$(pObject).attr("id")+"'":"")+", 'START')");
	}

	var parameters = serialize($(pObject));

	var couples = parameters.split('&');
	var tmp; var myData = {};
	for(i=0; i<couples.length; i++) {
		if (couples[i] != undefined && couples[i] != '') {
			tmp = couples[i].split('=');
			myData[tmp[0]] = tmp[1];
		}
	}

	$.ajax({
		url:pUrl,
		data:myData,
		type:'POST',
		success:function (result){
			if (result.substr(0,2) == 'KO') {
				if (pFunction != undefined && pFunction != '') {
					eval(pCallbpFunctionack+"("+($(pObject).attr("id")?"'#"+$(pObject).attr("id")+"'":"")+", 'FAIL', "+result+")");
				}
			} else {
				if (pFunction != undefined && pFunction != '') {
					eval(pFunction+"("+($(pObject).attr("id")?"'#"+$(pObject).attr("id")+"'":"")+", 'SUCCESS', "+result+")");
				}
			}
		}
	})
}
/* ****************************************************************** */
/* ****************************************************************** */
/* ****************************************************************** */
function loadContent(pWhere, pContent, pParams, pFunction) {
    $.ajax({
        type: "POST",
        url: "core/ajax.loadContent.php?c="+pContent,
        data: pParams,
        success: function(data, txtStatus, xmlReq){
            pWhere.append(data);
            if (pFunction != '' && pFunction != undefined) {
                eval(pFunction+"($('#"+$(pWhere).attr('id')+"'))");
            }
        }
    });
}