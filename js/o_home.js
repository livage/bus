	$(document).ready(function(){
	$('#txtSource, #txtDestination').autocomplete( // Autocomplete - Search behaviour
		"getstation.php",{
		//minChars: 3, // min nbr of char before autocomplete fire
		//minChars: 3, // min nbr of char before autocomplete fire
		max: 30, // max nbr of results shown
		delay: 100, // time to wait before autocomplete fire
		autoFill: false, // flag to decide if while the user write the word is completing
		mustMatch: false, // flag to decide if accept only values shown in the list
		matchContains: true, //
		scrollHeight: 300, // hieght of the list showed
		formatItem: function(row, i, max) { // Function to format the values when found
			var retStr;
			if (row.length > 1) {
				retStr = row[1]+" ("+row[0]+")";
			// retStr += row[2] + "<br />" + "<br />"+ "<br />";
			} else
			retStr = row[1];
			return retStr; // Return HTML code to show foreach row
		},
		formatResult: function(row) { //Function returning the value to put on the field
			if (row.length > 1)  // e.g. if something found
			//return row[0];  //   first field
			 return row[1];  //   first field
			// $('#Destination').val(row[1]);

			else
			return '';   //   else blank string
		}
	});
	
	$('#txtSource, #txtDestination').result(function(event, data, formatted) { // Autocomplete - Result behaviou

		if (data) { // If something selected
		
			$(this).val(data[1]);
			
			if($(this).attr('id') == 'txtDestination' )
			$('#Destination').val(data[0]);
		
		if($(this).attr('id') == 'txtSource' )
			$('#Source').val(data[0]);
			
		}
	});

				$('#txtOnwardCalendar').mask('99/99/9999');
				$('#txtOnwardCalendar').datepicker({
					dateFormat: 'dd/mm/yy',
					dayNames: ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
					dayNamesMin: ['Su','Mo', 'Tu', 'We', 'Th', 'Fi', 'Sa' ],
					//dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
		//			monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
					monthNames: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
					monthNamesShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
					firstDay: 1,
					minDate:0,
					gotoCurrent:true,
					//showOn: 'button',
					showAnim: 'fadeIn',
					showOptions: {duration: 300},
					//buttonImage: 'images/icons/calendar.png',
					buttonText: 'Clicca qui per visualizzare il calendario',
					buttonImageOnly: true
		     	});
					$('#txtReturnCalendar').mask('99/99/9999');
				$('#txtReturnCalendar').datepicker({
					dateFormat: 'dd/mm/yy',
					dayNames: ['Sunday','Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ],
					dayNamesMin: ['Su','Mo', 'Tu', 'We', 'Th', 'Fi', 'Sa' ],
					//dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
		//			monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
					monthNames: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
					monthNamesShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
					firstDay: 1,
					minDate:0,
					gotoCurrent:true,
					//showOn: 'button',
					showAnim: 'fadeIn',
					showOptions: {duration: 300},
					//buttonImage: 'images/icons/calendar.png',
					buttonText: 'Clicca qui per visualizzare il calendario',
					buttonImageOnly: true
		     	});
			


	});




