<?php
/* 	***********************************************************************
	doUpload
	----------------------------------------------------------------------
	----------------------------------------------------------------------
	$prmFILES					$_FILE variable
	$prmSizes					sizes to create for the uploaded image.
								"ATT"		: saved as no image file
								"IMG"		: saved as image file (no resize process)
								"WxH:WxH.."	: saved as image file (with resize process).
											  Sizes delimited by ":". Width & Height delimited by "x".
	$prmPath					path to save file
	$prmFilenamePattern			pattern for the new file name(s):
								$SIZE$		: replace pattern with image size order (only for images)
								$FILENAME$	: originale client file name (without path)
								$-x-$ 		: for user values, where x represent the positional parameter;
								i.e. IMG_$-1-$_$-2-$_$SIZE$_$FILENAME$
	$prmFilenamePatternValues	positional  array of values to substitute user values
	$prmError					string reporting errors
	$prmExtensions				string of allowed extensions splitted by ";". "" means every extension allowed
	$prmId						object id to be passed
	$prmRatio					ratio of the new images:
								-1 			: maintain aspect ration
								other value	: apply the given aspect ratio (x/y)
	*********************************************************************** */


function doUpload(	$prmFILES,
					$prmSizes="IMG",
					$prmPath,
					$prmFilenamePattern,
					$prmFilenamePatternValues=array(),
					&$prmError,
					$prmAllowedExtensions="",
					$prmId="",
					$prmRatio=-1) {

	require_once("func.createFilename.php");

	$prmError = "";

	// Get extension and check if allowed
	if (strlen($prmAllowedExtensions)) {
		// Get image extension
		$myExt = pathinfo($prmFILES['name'], PATHINFO_EXTENSION);
    	$allowedExtensions = explode(";", $prmAllowedExtensions);
    	$isAllowedExtension = (array_search($myExt, $allowedExtensions) !== false);
	} else {
		$isAllowedExtension = true;
	}

	if ($isAllowedExtension) {

		if ($prmSizes=="ATT" || $prmSizes=="IMG") { // Se non � un'immagine o � un'immagie da non ridimensionare

			$targetFilename = createFileName($prmFILES["name"], 0, $prmFilenamePattern, $prmFilenamePatternValues, $prmId);

			if (!move_uploaded_file($prmFILES['tmp_name'], $prmPath.$targetFilename)) {
				if (!copy($prmFILES['tmp_name'], $prmPath.$targetFilename))
					return false;
				else
					return true;
			} else
				return true;

		} elseif ($prmSizes != "") { // Se � un'immagine

		    // Read image attributes
		    $imageAttr = getimagesize($prmFILES['tmp_name']);
		    list($dim_x, $dim_y, $imageType, $htmlTag) = $imageAttr;

		    $mySizes = explode(":", $prmSizes);

		    // Define uploaded image size
		    for ($s=0; $s<count($mySizes); $s++) {
				list($actWidth, $actHeight) = explode("x", $mySizes[$s]);
				if ($actWidth && $actHeight) {
			        if ($actWidth && ($dim_x > $actWidth)) { // If over width
			            if ($dim_x/$actWidth > $dim_y/$actHeight) { // width more over than height
			                $myWidth[$s] = $actWidth;
			                if ($prmRatio == -1)
			                	$myHeight[$s] = $dim_y * $actWidth / $dim_x;
			               else
			               		$myHeight[$s] = $actWidth * $prmRatio;
			            } else { // height more over than width
			            	if ($prmRatio == -1)
			                	$myWidth[$s] = $dim_x * $actHeight / $dim_y;
			                else
			                	$myWidth[$s] = $actHeight * $prmRatio;
			                $myHeight[$s] = $actHeight;
			            }

			        } elseif ($actHeight && ($dim_y > $actHeight)) { // If over height
			            if ($prmRatio)
			        		$myWidth[$s] = $dim_x * $actHeight / $dim_y;
			        	else
			        		$myWidth[$s] = $actWidth;
			            $myHeight[$s] = $actHeight;

			        } else {
			        	$myWidth[$s] = $dim_x;
			            $myHeight[$s] = $dim_y;
			        }
				} else {
					$myWidth[$s] = $dim_x;
			        $myHeight[$s] = $dim_y;
				}

		        $targetFilename[$s] = createFileName($prmFILES["name"], $s, $prmFilenamePattern, $prmFilenamePatternValues, $prmId);
		    }

	        //
	        switch ($imageType) {
	        	case IMAGETYPE_IFF:
	        	case IMAGETYPE_JB2:
	        	case IMAGETYPE_JP2:
	        	case IMAGETYPE_JPC:
	        	case IMAGETYPE_JPX:
	        	case IMAGETYPE_PSD:
	        		break;
	        	case IMAGETYPE_SWC:
	        	case IMAGETYPE_SWF:

	        		break;
	        	case IMAGETYPE_TIFF_II:
	        	case IMAGETYPE_TIFF_MM:
					break;
	        	case IMAGETYPE_GIF:
	        			$img_uploaded = imagecreatefromgif($prmFILES['tmp_name']);
	        		break;

	            case IMAGETYPE_JPEG:
	        	case IMAGETYPE_JPEG2000:
	                    $img_uploaded = imagecreatefromjpeg($prmFILES['tmp_name']);
	                break;

	            case IMAGETYPE_PNG:
	                    $img_uploaded = imagecreatefrompng($prmFILES['tmp_name']);
	                    break;

	            case IMAGETYPE_BMP:


	            case IMAGETYPE_WBMP:
	                    $img_uploaded = imagecreatefromwbmp($prmFILES['tmp_name']);
	                break;

	           	case IMAGETYPE_XBM:
	           			$img_uploaded = imagecreatefromxbm($prmFILES['tmp_name']);
	           		break;
	        };

	        if (!$img_uploaded) {
	            $prmError = "Error creating uploaded file memory image";
	            return false;
	        }

	        // Create target image
	        $imageCreated = true;
	        for ($s=0; $s<count($mySizes); $s++) {
		        $imageNew = imagecreatetruecolor($myWidth[$s], $myHeight[$s]);
		        if (!$imageNew) {
		            $prmError = "Error creating new blank image";
		            return false;
		        }
		        if (!imagecopyresampled($imageNew, $img_uploaded, 0, 0, 0, 0, $myWidth[$s], $myHeight[$s], $dim_x, $dim_y)) {
		            $prmError = "Error resampling image";
		            return false;
		        }

		        // Write image
		        switch ($imageType) {
		            case IMAGETYPE_JPEG:
		            case IMAGETYPE_JPEG2000:
		                    $imageCreated &= imagejpeg($imageNew, $prmPath.$targetFilename[$s], 100);
		                break;

		            case IMAGETYPE_PNG:
		                    $imageCreated &= imagepng($imageNew, $prmPath.$targetFilename[$s]);
		                break;

		            case IMAGETYPE_WBMP:
		                    $imageCreated &= imagewbmp($imageNew, $prmPath.$targetFilename[$s]);
		                break;

		            case IMAGETYPE_GIF:
		            		$imageCreated &= imagegif($imageNew, $prmPath.$targetFilename[$s]);
		            	break;

		            case IMAGETYPE_XBM:
	           				$imageCreated &= imagexbm($imageNew, $prmPath.$targetFilename[$s]);
	           			break;
		        };
	        }

	        if (!$imageCreated) {
	            $prmError = "Error writing target image";
	            return false;
	        }

	        return $imageCreated;
		}

	} else { // No allowed extension
		$prmError = "Not Allowed Extension";
		return false;

	}
}
?>