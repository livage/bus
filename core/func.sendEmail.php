<?php
/* '<([A-Z][A-Z0-9]*)[^>]*>(.*?)</\1>' */
function sendEmail ($pmData, $pmDebug=0) {

    require_once('libs/phpmailer/class.phpmailer.php');

    $targets = array();

    // Legge i dati dai parametri
    foreach ($pmData as $param => $value) {
    	switch (strtoupper($param)) {
    		case 'FROM':
    				$lc_source = array(	'email'	=>	$value['email'],
				        				'name'	=>	$value['name']);
    			break;
    		case 'TO':
    		case 'CC':
    		case 'BCC':
    		case 'REPLY-TO':
    				if (array_key_exists('0', $value)) {
				        for ($e=0; $e<count($value); $e++)
				        	$lc_targets[$param][] = array(	'email'	=>	$value[$e]['email'],
				        									'name'	=>	$value[$e]['name']);
    				} else {
						$lc_targets[$param][] = array(	'email'	=>	$value['email'],
				        								'name'	=>	$value['name']);
				    }
    			break;
    		case 'PRIORITY':
    				$lc_priority = $value;
    			break;
    		case 'SUBJECT':
    				$lc_subject = $value;
    			break;
    		case 'BODY':
    				list($lc_bodyType, $lc_bodySource) = explode(':', $value['SOURCE'], 2);
    			break;
    		case 'ATTACHS':
    				if (array_key_exists('0', $value)) {
				        for ($e=0; $e<count($value); $e++)
				        	$lc_attachs[] = array(	'filename'		=> $value[$e]['file'],
				        							'attachname'	=> $value[$e]['attach']);
    				} else {
						$lc_attachs[] = array(	'filename'		=> $value['file'],
				        						'attachname'	=> $value['attach']);
				    }
    			break;
    		case 'IMAGES':
    				if (array_key_exists('0', $value)) {
    					for ($e=0; $e<count($value); $e++){
    						$lc_images[] = array(
    							'id' 	=> $value[$e]['id'],
    							'image'	=> $value[$e]['filename'],
    						);
    					}
    				} else {
						$lc_images[] = array(
							'id' 	=> $value['id'],
							'image'	=> $value['filename'],
						);
    				}
    			break;
    		case 'SMTP':
//    				if (is_array($value[0]) && count($value)) {
//				        for ($h=0; $h<count($value); $h++)
//				        	$lc_hosts[] = array(	'host'	=> $value[$h]['hostname'],
//				        							'user'	=> $value[$h]['username'],
//				        							'pass'	=> $value[$h]['password']);
//    				} else {
//						$lc_hosts[] = array(	'host'	=> $value['hostname'],
						$lc_hosts = array(	'host'	=> $value['hostname'],
				        					'user'	=> $value['username'],
				        					'pass'	=> $value['password']);
//				    }
    			break;
    		case 'RETURN-TO':
    				$lc_returnTo = $value;
    			break;
    	}
    }

    if ($lc_source && count($lc_targets)) {

	    $mail = new PHPMailer();
	    $mail->IsSMTP();
	    $mail->IsHTML($lc_bodyType == 'HTML');

//	    $mail->SMTPDebug = true;
		$mail->AddCustomHeader('X-Mailer: PHPMailer [version 1.x]');
		if ($lc_returnTo) $mail->AddCustomHeader('X-Sender: $lc_returnTo');
		$mail->AddCustomHeader('X-AntiAbuse: User - $prmFrom');
		$mail->AddCustomHeader('X-AntiAbuse: This is a solicited email for - '.SITE_NAME.' mailinglist.');
		$mail->AddCustomHeader('X-AntiAbuse: Servername - '.$_SERVER['SERVER_NAME']);
		$mail->AddCustomHeader('Organization: '.SITE_NAME);
		$mail->AddCustomHeader('X-MimeOLE: Produced By Microsoft MimeOLE V6.00.2800.1441');

	    // From
	    $mail->From     = $lc_source['email'];
	    $mail->FromName = $lc_source['name'];

	    foreach ($lc_targets as $type => $emails) {
	    	switch ($type) {
	    		case 'TO':
	    				for ($e=0; $e<count($emails); $e++)
	    					if ($emails[$e]['email'])
	    						$mail->AddAddress($emails[$e]['email'], $emails[$e]['name']);
	    			break;
	    		case 'CC':
	    				for ($e=0; $e<count($emails); $e++)
	    					if ($emails[$e]['email'])
	    						$mail->AddCC($emails[$e]['email'], $emails[$e]['name']);
	    			break;
	    		case 'BCC':
	    				for ($e=0; $e<count($emails); $e++)
	    					if ($emails[$e]['email'])
	    						$mail->AddBCC($emails[$e]['email'], $emails[$e]['name']);
	    			break;
	    		case 'REPLY-TO':
	    				for ($e=0; $e<count($emails); $e++)
	    					if ($emails[$e]['email'])
	    						$mail->AddReplyTo($emails[$e]['file'], $emails[$e]['name']);
	    			break;
	    	}
	    }

		// Priority
		if ($prmPriority) $mail->Priority = $prmPriority;

		// Subject
	    $mail->Subject = $lc_subject;

	    // Body
	    $mail->Body = $lc_bodySource;
//	    $mail->Body = $prmBody["text"];
//
//	    switch ($prmBody["mode"]) {
//	        case "HTML":
//	                $mail->IsHTML(true);
//	                $mail->AltBody = nl2br(strip_tags(str_replace(array(" ", "\t", "\n", "\r"), array(), ($prmBody["text"]))));
//	            break;
//
//	        case "TEXT":
//	                $mail->IsHTML(false);
//	            break;
//	    }

	    // Attachments
	    if (is_array($lc_attachs) && count($lc_attachs)) {
	        for ($a=0; $a<count($lc_attachs); $a++)
	            $mail->AddAttachment($lc_attachs[$a]["filename"], $lc_attachs[$a]["attachname"]);
	    }

	    // Images
	    if (is_array($lc_images) && count($lc_images)) {
	    	for ($a=0; $a<count($lc_images); $a++) {
//	    		echo ($lc_images[$a]["image"].'-'.$lc_images[$a]["id"].'-'.basename($lc_images[$a]["image"]));
	            $mail->AddEmbeddedImage($lc_images[$a]["image"], $lc_images[$a]["id"], basename($lc_images[$a]["image"]));
	    	}
	    }

	    // Mail Server
	    $mail->Host = $lc_hosts['host'];

	    if ($lc_hosts['user']) {
	        $mail->SMTPAuth = true;
	        $mail->Username = $lc_hosts['user'];
	        $mail->Password = $lc_hosts['pass'];
	    } else {
	        $mail->SMTPAuth = false;
	    }

	    if ($mail->Send())
	        return true;
	    else
	        return $mail->ErrorInfo;
    }
}
?>