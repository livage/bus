<?php
function mysqlTime($prmTime) {
if ($prmTime) {
    
         // Elabora l'ora
        list($ora, $minuti) = explode(":", $prmTime);
        if (	is_numeric($ora) && $ora >= 0 && $ora < 24
        	&& 	is_numeric($minuti) && $minuti >= 0 && $minuti < 60)
        	 {
            $retValue = "'".str_pad($ora, 2, "0", STR_PAD_LEFT).":".
                        str_pad($minuti, 2, "0", STR_PAD_LEFT)."'";
        } else
            $retValue = "NULL";
    } else
        $retValue = "NULL";

echo $retValue;
return $retValue;

}
?>