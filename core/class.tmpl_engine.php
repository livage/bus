<?php
//require_once("cfg/inc.config.php");
require_once("core/inc.config.php");
define("SMARTY_DIR", LOCAL_ROOT_PATH.'libs/smarty/');

require(SMARTY_DIR."Smarty.class.php");

class T_tmpl_engine extends Smarty {

   function T_tmpl_engine()
   {

        // Class Constructor.
        // These automatically get set with each new instance.

        $this->Smarty();

        $this->template_dir = LOCAL_ROOT_PATH.'htmls/';
        $this->compile_dir  = LOCAL_ROOT_PATH.'htmls/templates_c/';
        $this->config_dir   = LOCAL_ROOT_PATH.'htmls/configs/';
        $this->cache_dir    = LOCAL_ROOT_PATH.'htmls/cache/';

        $this->clear_all_cache();
        $this->caching = false;
		$this->force_compile = true;
        $this->compile_check = true;
        $this->debug_tpl = LOCAL_ROOT_PATH.'/libs/smarty/debug.tpl';
        $this->debugging_ctrl = 'URL';
       //$this->debugging = true;
     	$this->debugging = defined('SMARTY_DEBUG')?SMARTY_DEBUG:false;
//        echo ("compile_dir: ".$this->compile_dir."<br />");
//        echo ("cache_dir: ".$this->cache_dir."<br />");

   }
};
?>