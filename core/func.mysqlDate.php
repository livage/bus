<?php
function mysqlDate($prmDate) {

if ($prmDate) {
    list ($giorno, $mese, $anno) = explode("/", $prmDate);
    if (	is_numeric($mese)
    	&&	is_numeric($giorno)
    	&&	is_numeric($anno)
    	&&	checkdate($mese, $giorno, $anno)) {
        $retValue = "'$anno-$mese-$giorno'";
    } else
        $retValue = "NULL";
} else
    $retValue = "NULL";

return $retValue;

}
?>