<?php
function doOperation($connection, $operation, $table, $fieldMap, $posted, &$sqlError, $id=0) {
	require('inc.strip_all.php');

	switch ($operation) {
		case 'I':
				foreach ($posted as $field => $value) {
					
					if (!is_array($value)) {
					
						/* STRING */
						if (in_array($field, $fieldMap['STRING'])) {
							require_once('func.mysqlPrepare.php');
							$sqlFields .= "$field, ";
							$sqlValues .= '\''.mysqlPrepare($value).'\', ';

						/* INTEGER */
						} elseif (in_array($field, $fieldMap['INT'])) {
							$sqlFields .= "$field, ";
							$sqlValues .= intval($value).', ';

						/* FLOAT */
						} elseif (in_array($field, $fieldMap['FLOAT'])) {
							$sqlFields .= "$field, ";
							$sqlValues .= floatval($value).', ';

						/* DATE */
						} elseif (in_array($field, $fieldMap['DATE'])) {
							require_once('func.mysqlDate.php');
							$sqlFields .= "$field, ";
							$sqlValues .= mysqlDate($value).', ';

						/* DATE TIME */
						} elseif (in_array($field, $fieldMap['DATETIME'])) {
							require_once('func.mysqlDateTime.php');
							$sqlFields .= "$field, ";
							$sqlValues .= mysqlDateTime($value).', ';
						}
						/* TIME */
						 elseif (in_array($field, $fieldMap['TIME'])) {
						
							require_once('func.mysqlTime.php'); 
							$sqlFields .= "$field, ";
							$sqlValues .= mysqlTime($value).', ';
							
						}
					}
				}
				$sql = "INSERT INTO $table (
					$sqlFields
					company,
					insU,
					insTS
				) VALUES (
					$sqlValues
					'".$_SESSION[SITE_NAME]['login_data']['company']."',
					'".$_SESSION[SITE_NAME]['login_data']['username'].'\',
					NOW()
				)';
				
				//echo $sql;exit;
			break;
		case 'U':
				foreach ($posted as $field => $value) {
					if (!is_array($value)) {
						/* STRING */
						if (in_array($field, $fieldMap['STRING'])) {
							require_once('func.mysqlPrepare.php');
							$sqlSet .= "$field = '".mysqlPrepare($value)."', ";

						/* INTEGER */
						} elseif (in_array($field, $fieldMap['INT'])) {
							$sqlSet .= "$field = ".intval($value).", ";

						/* FLOAT */
						} elseif (in_array($field, $fieldMap['FLOAT'])) {
							$sqlSet .= "$field = ".floatval($value).", ";

						/* DATE */
						} elseif (in_array($field, $fieldMap['DATE'])) {
							require_once('func.mysqlDate.php');
							$sqlSet .= "$field = ".mysqlDate($value).", ";

						/* DATE TIME */
						} elseif (in_array($field, $fieldMap['DATETIME'])) {
							require_once('func.mysqlDateTime.php');
							$sqlSet .= "$field = ".mysqlDateTime($value).", ";
						}
						/* TIME */
						elseif (in_array($field, $fieldMap['TIME'])) {
							require_once('func.mysqlTime.php');
							$sqlSet .= "$field = ".mysqlTime($value).", ";
						}
					}
				}
				$sql = "UPDATE	$table
							SET $sqlSet
							updU = '".$_SESSION[SITE_NAME]['login_data']['username']."',
							updTS = NOW()
					WHERE 	id = $id";
					//echo $sql;exit;
			break;
		case 'D':
				$sql = "DELETE FROM $table WHERE id = $id";
			break;
		case 'S':
				$sql = "UPDATE $table SET active = 1-active WHERE id = $id";
			break;
	}
//echo $_SESSION[SITE_NAME]['login_data']['username'];
	return sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
}
?>