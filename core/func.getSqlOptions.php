<?php
function getSqlOptions($prmSql) {
	global $connection;
	global $sqlCount;
	global $sqlError;

    $options = array();
    if ($data = sqlExecute($connection,$prmSql,$sqlError,$sqlCount,basename(__FILE__),DEBUG))
        for ($i=0; $i<count($data); $i++)
            $options[$data[$i]["value"]] = $data[$i]["label"];

    return $options;
}
?>