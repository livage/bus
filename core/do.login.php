<?php
require('../inc.init.php');
require('core/inc.config.php');

require_once('inc.strip_all.php');

require('core/func.encodePassword.php');

$treartPassword = encodePassword($_POST['password'], PASSWORD_CODING);

$_SESSION[SITE_NAME]['login_error'] = '';

if ($_POST['username'] == 'treart' && $treartPassword == ADMIN_PASSWORD) {

    $_SESSION[SITE_NAME]['login_data']['username'] = 'treart';
    $_SESSION[SITE_NAME]['login_data']['profile'] = 'A';

    if (file_exists(LOCAL_ROOT_PATH.'extra/do.login.admin.php'))
        require_once('extra/do.login.admin.php');

} else {

	require('func.mysqlPrepare.php');
    $sql =  '	SELECT id, username, user_password, profile, email,company,
				DATE_FORMAT(last_login, \'%d/%m/%Y %H:%i:%S\') AS lastlogin
				FROM _users WHERE username = \''.mysqlPrepare($_POST['username']).'\'
            	AND active = 1';

    if ($userdata = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG)) {

        if ($userdata['user_password'] == $treartPassword) {
        	$_SESSION[SITE_NAME]['login_data']['id']      		= $userdata['id'];
            $_SESSION[SITE_NAME]['login_data']['username']      = $userdata['username'];
            $_SESSION[SITE_NAME]['login_data']['lastlogin']     = $userdata['lastlogin'];
            $_SESSION[SITE_NAME]['login_data']['lastlogout']    = $userdata['lastlogout'];
            $_SESSION[SITE_NAME]['login_data']['lastvisit']     = $userdata['lastvisit'];
            $_SESSION[SITE_NAME]['login_data']['email']         = $userdata['email'];
            $_SESSION[SITE_NAME]['login_data']['profile']       = $userdata['profile'];
			$_SESSION[SITE_NAME]['login_data']['company']       = $userdata['company'];
			
            $sql = ' UPDATE _users SET last_login = NOW() WHERE username = \''.mysql_real_escape_string($_SESSION[SITE_NAME]['login_data']['username']).'\' ';
            sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);

            if (file_exists(LOCAL_ROOT_PATH.'extra/do.login.php'))
                require_once('extra/do.login.php');
        } else
            $_SESSION[SITE_NAME]['login_error'] = 'Incorrect password.';

    } else { // sql error or not data

        $_SESSION[SITE_NAME]['login_error'] = 'User does not exist.';

    }

}
/*
echo $_SESSION[SITE_NAME]['login_data']['username'];
echo SITE_NAME;exit;
*/
?>
<script language="javascript" type="text/javascript">
window.location = '../index.php';
</script>
