<?php
function sqlExecute($prmConnection, $prmSql, &$prmError, &$prmSQLCount, $prmFileDetail, $prmDebug=0) {
	global $_TREART;
    $prmSQLCount++;
    $error = 0;

    // Considera
    if (is_array($prmSql)) {
        foreach ($prmSql as $param => $value) {
            switch ($param) {
                case "rowFrom":
                        $limitFrom = $value;
                    break;

                case "rowLimit":
                        $limitRange = $value;
                    break;

                case "sqlStatement":
                        $sql = $value;
                    break;
                case "getModel":
                        $getStructure = $value;
                    break;
            }
        }

        if ($limitRange) {
            $sql .= " LIMIT ".(intval($limitFrom)).", ".(intval($limitRange));
        }

    } else {
        $sql = $prmSql;
    }

    // Determina il tipo di operazione da eseguire
	$tmp = strtoupper(trim($sql));
	$i=0;
	while ($i<strlen($tmp) && (substr($tmp, $i, 1) < 'A' || substr($tmp, $i, 1) > 'Z')) {
		$i++;
    }
    $f=$i;
    while ($f<strlen($tmp) && (substr($tmp, $f, 1) >= 'A' && substr($tmp, $f, 1) <= 'Z')) {
		$f++;
    }
//	$command = trim(substr($tmp, $i, (strpos($tmp, ' ', $i))-$i));
	$command = trim(substr($tmp, $i, $f-$i+1));

    switch ($command) {
        case "SELECT":
                if ($rs = mysql_query($sql, $prmConnection)) {
                    if ($getStructure) {
                        for ($i=0; $i<mysql_num_fields($rs); $i++) {
                            $structure[mysql_field_name($rs, $i)]["type"] = mysql_field_type($rs, $i);
                            $structure[mysql_field_name($rs, $i)]["len"] = mysql_field_len($rs, $i);
                            $structure[mysql_field_name($rs, $i)]["flag"] = mysql_field_flags($rs, $i);
                        }
                    }

                    switch ($limitRange) {

                        case 1:
                                $result = mysql_fetch_assoc($rs);
                            break;

                        default:
                                $i = 0;
                                while ($data = mysql_fetch_assoc($rs)) {
                                    $result[$i++] = $data;
                                }
                            break;
                    }
                } else {
                    $message = mysql_error();
                    $error = 1;
                }
            break;

        case "INSERT":
                if (mysql_query($sql, $prmConnection)) {
                    $result = mysql_insert_id();
                    $message = "INSERT ID: ".$result;
                } else {
                    $result = false;
                    $message = mysql_error();
                    $error = 1;
                }
            break;

        case "UPDATE":
        case "DELETE":
                if (mysql_query($sql, $prmConnection)) {
                    $result = mysql_affected_rows();
                    $message = "AFFECTED ROWS: ".$result;
                } else {
                    $result = false;
                    $message = mysql_error();
                    $error = 1;
                }
            break;

        default:
                if (!mysql_query($sql, $prmConnection)) {
                    $result = false;
                    $message = mysql_error();
                    $error = 1;
                }
            break;
    }

    if ($prmDebug) {
        require_once("func.writeLog.php");
        writeLog($prmConnection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], $sql, $message, $_TREART, $prmFileDetail, 'S', $error );
    }

    if ($getStructure) {
        return array("result" => $result, "model" => $structure);
    } else
        return $result;

}
?>