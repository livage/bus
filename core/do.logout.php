<?php
require("../inc.init.php");
require("core/inc.config.php");

session_destroy();
unset($_SESSION[SITE_NAME]);
//session_unregister("login_data");

header("Location: ../index.php");
?>
