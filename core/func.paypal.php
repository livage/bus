<?php

function paypal($prmAzione = "process", $prmLang, $prmEveto, $prmCosto) {
	// Setup class
	require_once('paypal.class.php');  // include the class file
	require_once("inc.translations.php");
	$p = new paypal_class;             // initiate an instance of the class
//	$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
	$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url

	// if there is not action variable, set the default action of 'process'
//	if (empty($_GET['action'])) $_GET['action'] = 'process';

	switch ($prmAzione) {

		case 'process':      // Process and order...

			//      $p->add_field('business', 'YOUR PAYPAL (OR SANDBOX) EMAIL ADDRESS HERE!');
//			$p->add_field('business', 'luca_1202995610_biz@treart.it');
			$p->add_field('business', 'info@italiausa.org');
			//$p->add_field('return', 'http://www.italiausa.org/result.php?action=success&lang='.$prmLang);
			$p->add_field('cancel_return', 'http://www.italiausa.org/index.php?c=iscrizione-online');
			//$p->add_field('notify_url', 'http://www.italiausa.org/result.php?action=ipn&lang='.$prmLang);
			$p->add_field('item_name', $prmEveto);
			$p->add_field('amount', $prmCosto);
			$p->add_field('mc_currency','EUR');
			$p->add_field('currency_code','EUR');
			$p->add_field('no_shipping','1');
			$p->add_field('lc', ($prmLang=="en"?"IT":"US"));
			$p->add_field('first_name', $_POST['shipping_name']);
			$p->add_field('address1', $_POST['shipping_address']);

			$p->submit_paypal_post($prmLang); // submit the fields to paypal
			//$p->dump_fields();      // for debugging, output a table of all the fields
			break;

		case 'success':      // Order was successful...

			// This is where you would probably want to thank the user for their order
			// or what have you.  The order information at this point is in POST
			// variables.  However, you don't want to "process" the order until you
			// get validation from the IPN.  That's where you would have the code to
			// email an admin, update the database with payment status, activate a
			// membership, etc.

//			echo "<html><head><title>". $traduzioni[$prmLang]["success"]."</title></head><body><h3>". $traduzioni[$prmLang]["grazie"] ."</h3>";
//			sort($_POST,SORT_STRING);
			foreach ($_POST as $key => $value) {
				switch ($key){
					case "payment_date":
						$data = strftime("%d/%m/%Y %H:%M:%S",strtotime($value));
						break;
					case "first_name":
						$nome = $value;
						break;
					case "last_name":
						$Cognome = $value;
						break;
					case "item_name":
						$item = $value;
						break;
					case "mc_currency":
						$curr = $value;
						break;
					case "mc_gross":
						$costo = $value;
						break;
				}
			}

			$tabella = "<p class=\"titolo_pay\">".$traduzioni[$prmLang]["grazie"] ."</p><div class=\"torna_pay\">
			<table class='paypaltable'>
			<tr>
			<td>".$traduzioni[$prmLang]["datapag"]."</td>
			<td>$data</td>
			</tr>
			<tr>
			<td>".$traduzioni[$prmLang]["nome"]."</td>
			<td>$nome $Cognome</td>
			</tr>
			<tr>
			<td>".$traduzioni[$prmLang]["oggetto"]."</td>
			<td>$item</td>
			</tr>
			<tr>
			<td>".$traduzioni[$prmLang]["costo"]."</td>
			<td>$costo $curr</td>
			</tr>
			</table>
			<input type='button' value='Home Page' onClick='window.location=\"index1".($prmLang=="en"?"ing":"").".htm\"'></div>";
			echo str_replace('$$CONTENUTO$$', $tabella, file_get_contents("template4php.htm"));

			// You could also simply re-direct them to another page, or your own
			// order status page which presents the user with the status of their
			// order based on a database (which can be modified with the IPN code
			// below).

			break;

		case 'cancel':       // Order was canceled...

			$tabella =  "<center>". $traduzioni[$prmLang]["cancmsg"] ."<br /><br /><a href='index1".($prmLang=="en"?"ing":"").".htm'>&nbsp;&nbsp;&lt; ".$traduzioni[$prmLang]["backhome"]."</a></center></body></html>";
			echo str_replace('$$CONTENUTO$$', $tabella, file_get_contents("template4php_".$prmLang.".htm"));


			break;

		case 'ipn':          // Paypal is calling page for IPN validation...

			if ($p->validate_ipn()) {

				// Payment has been recieved and IPN is verified.  This is where you
				// update your database to activate or process the order, or setup
				// the database with the user's order details, email an administrator,
				// etc.  You can access a slew of information via the ipn_data() array.

				// Check the paypal documentation for specifics on what information
				// is available in the IPN POST variables.  Basically, all the POST vars
				// which paypal sends, which we send back for validation, are now stored
				// in the ipn_data() array.

				// For this example, we'll just email ourselves ALL the data.
				$subject = 'Notifica di pagamento immefdiato - Ricevimento pagamento';
				$to = MAILTO;    //  your email
				$body =  "Pagamento terminato con successo.\n";
				$body .= "from ".$p->ipn_data['payer_email']." on ".date('m/d/Y');
				$body .= " at ".date('g:i A')."\n\nDettagli:\n";

				foreach ($p->ipn_data as $key => $value)
					$body .= "\n$key: $value";
				mail($to, $subject, $body);
			}
			brek;
 }
}
?>
