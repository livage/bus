<?php
// DATABASE
require('cfg/sys.db.config.php');

$fileData = pathinfo(__FILE__);
$normalizedPath = str_replace('\\', '/', $fileData['dirname']);
define('LOCAL_ROOT_PATH',
        substr($normalizedPath, 0, strlen($normalizedPath)-strlen('core')));

// Search first "/" occurrence
$slashPos = strpos(LOCAL_ROOT_PATH, '/');

if (substr($_SERVER['DOCUMENT_ROOT'], strlen($_SERVER['DOCUMENT_ROOT'])-1 ,1) == '/')
	$documentRoot = $_SERVER['DOCUMENT_ROOT'];
else
	$documentRoot = $_SERVER['DOCUMENT_ROOT'].'/';
//echo "-0-".SITE_NAME.$_SESSION[SITE_NAME]['language']."--";
define ('SITE_NAME',
        substr( $normalizedPath,
                strlen($documentRoot),
                strlen($normalizedPath)-strlen('core')-strlen($documentRoot)-1)
);
//echo "-0-".SITE_NAME.$_SESSION[SITE_NAME]['language']."--";
if (SITE_NAME == '') {
	define('SITE_NAME', $_SERVER['HTTP_HOST']);
	define('WEB_FOLDER', '/');
} else
	define('WEB_FOLDER', SITE_NAME.'/');
//echo ("COST:".LOCAL_ROOT_PATH."-".SITE_NAME."-".WEB_FOLDER."-".$_SERVER['DOCUMENT_ROOT']."<br />");
// CONFIGURATION
$connection = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD);
mysql_select_db(DB_NAME);
require_once('core/func.sqlExecute.php');

//define('DEBUG', 1);
//define('DEBUG_MAIL', 0);
//echo "-1-".$_SESSION[SITE_NAME]['language']."--";
$sql = ' SELECT inivar, inivalue FROM _init WHERE active = 1 ';
if ($data = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG))
    for($i=0; $i<count($data); $i++)
        if (!defined($data[$i]['inivar']))
    	   define($data[$i]['inivar'], $data[$i]['inivalue']);
//echo "-2-".$_SESSION[SITE_NAME]['language']."--";
if (version_compare(PHP_VERSION, '5.3.0', '<=')) {
	if (!session_is_registered(SITE_NAME))
    	session_register(SITE_NAME);
}
//echo "-3-".$_SESSION[SITE_NAME]['language']."--";
if (defined('DEFAULT_PROFILE') && $_SESSION[SITE_NAME]['login_data']['profile'] == '') {
    $_SESSION[SITE_NAME]['login_data']['profile'] = DEFAULT_PROFILE;
}
//echo "-4-".$_SESSION[SITE_NAME]['language']."--";
if (defined('DEFAULT_LANGUAGE') && $_SESSION[SITE_NAME]['language'] == '') {
    $_SESSION[SITE_NAME]['language'] = DEFAULT_LANGUAGE;
}
//echo "-5-".$_SESSION[SITE_NAME]['language']."--";
//print_r($_SESSION[SITE_NAME]);
//$_S3SSION = &$_SESSION[SITE_NAME];
//$_S3LOGIN = &$_SESSION[SITE_NAME]["login_data"];
?>