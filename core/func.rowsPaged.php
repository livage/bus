<?php
function rowsPaged($pmSql, $pmConnection, &$pmSqlCount, &$pmPaging = array("curPage" => 1, "perPage" => 10), $pmDebug = 0) {

/* 	***************************** */
/*	**	Possibili valori di $pmPaging
/* 	***************************** */
/*	**	curPage <--
/*	**	totRows
/*	**	perPage <--
/*	**	pageSet <--
/*	**	totPages
/*	**	arPages
/* 	***************************** */

//	if (!$pmPaging["curPage"])
//		$pmPaging["curPage"] = 1;
	$sqlFromPos = strpos($pmSql, "FROM");

	$sqlHeader = substr($pmSql, 0, $sqlFromPos-1);
	$sqlBody = substr($pmSql, $sqlFromPos);

	$sqlCount = " SELECT COUNT(*) AS counter $sqlBody ";
	$pageData = sqlExecute($pmConnection, array("rowLimit"=>1, "sqlStatement"=>$sqlCount), $sqlError, $pmSqlCount, DEBUG);
	if ($pageData["counter"]) {

		$totPages = ceil($pageData["counter"]/$pmPaging["perPage"]);

		$minRange = max(1, $pmPaging["curPage"]-round(($pmPaging["pageSet"]/2)));
		$maxRange = min($totPages, $pmPaging["curPage"]+round(($pmPaging["pageSet"]/2)));
		$pmPaging = array(
			"curPage"	=> $pmPaging["curPage"],
			"perPage"	=> $pmPaging["perPage"],
			"totRows" 	=> $pageData["counter"],
			"totPages"	=> $totPages,
			"arPages"	=> range($minRange, $maxRange),
		);
		$sql = $pmSql." LIMIT ".(($pmPaging["curPage"]-1)*$pmPaging["perPage"]).", ".$pmPaging["perPage"];
		$elementi = sqlExecute($pmConnection,$sql,$sqlError,$pmSqlCount,DEBUG);

		return $elementi;
	} else
		return false;

}
?>