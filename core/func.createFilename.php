<?php
function createFileName($prmFilename, $prmSize, $prmFilenamePattern, $prmFilenamePatternValues=array(), $prmId = "") {

	$retFilename = str_replace(
		array('$SIZE$' ,'$FILENAME$', '$ID$'),
		array($prmSize, $prmFilename, $prmId),
		$prmFilenamePattern
	);

	$trovato = true; $endPos = 0; $arReplaces = array();
	do {
		$trovato = strpos($prmFilenamePattern, "$-", $endPos);
		if ($trovato !== false) {
			$endPos = strpos($prmFilenamePattern, "-$", $trovato);
			$idx = substr($prmFilenamePattern, $trovato+2, $endPos-$trovato-2);
			$retFilename = str_replace("$-$idx-$", $prmFilenamePatternValues[$idx], $retFilename);
		}
	} while ($trovato);

	return $retFilename;
}

?>