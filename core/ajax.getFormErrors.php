<?php
require 'inc.init.php';
require 'inc.config.php';
require 'func.mysqlPrepare.php';

// Read the error message
$sql = ' SELECT code, message FROM _messages WHERE language = \''.mysqlPrepare($_POST['lang']).'\' AND active = 1 ';
$error = sqlExecute($connection,
                    $sql,
                    $sqlError,
                    $sqlCount,
                    basename(__FILE__),
                    DEBUG);
$retStr = '';
for ($e=0; $e<count($error); $e++) {
	$retStr .= $error[$e]['code'].'-->'.$error[$e]['message'].'||';
}

echo substr($retStr, 0, strlen($retStr)-2);
?>