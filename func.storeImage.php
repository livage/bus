<?php
function storeImage($rowId,$imageInfo,$imgType,$sizes=array()) {
// Take the file info data
	$arFileData = pathinfo($imageInfo['name']);


	$fileName = $arFileData['filename'];
	$fileExt = $arFileData['extension'];
	if (is_array($sizes)) {

		foreach ($sizes as $prefix => $dims) {

			// Set File name
			$newFileName = $imgType.'_'.$rowId.'_'.$prefix.'_'.$imageInfo['name'];

			// Set max dimensions
			list($maxX, $maxY) = explode('x', $dims);

			// Get image sizes
			list($sizeX, $sizeY) = getimagesize($imageInfo['tmp_name']);

			switch (strtolower($fileExt)) {
				case "png":
						$createFromFile = "imagecreatefrompng";
						$writeToDisk = "imagepng";
						break;
				case "jpg":
						$createFromFile = "imagecreatefromjpeg";
						$writeToDisk = "imagejpeg";
					break;
				case "gif":
						$createFromFile = "imagecreatefromgif";
						$writeToDisk = "imagegif";
					break;
			}

			// Create my image resource from the file
			$srcImg = $createFromFile($imageInfo['tmp_name']);

			// If greater size is less the the maximum
			$maxSize = max($maxX, $maxY);
			if ($maxSize == 0 || max($sizeX, $sizeY) <= $maxSize) { // nothing
				$newSizeX = $sizeX;
				$newSizeY = $sizeY;
			} else { // image to resize
				// compute the image aspect ratio
				$ratio = $sizeX/$sizeY;
				// check which dimension is more over
				if ($sizeX/$maxX > $sizeY/$maxY) {
					$newSizeX = $maxX;
					$newSizeY = $maxX/$ratio;
				} else {
					$newSizeY = $maxY;
					$newSizeX = $maxY*$ratio;
				}
			}

			$destImg = imagecreatetruecolor(round($newSizeX), round($newSizeY));

			// Resize the image
			imagecopyresampled($destImg,$srcImg,0, 0,0, 0,$newSizeX,$newSizeY,$sizeX,$sizeY);

		  	$writeToDisk($destImg, UPLOAD_PATH.$newFileName);

		}

	}

}
?>