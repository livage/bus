<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');



/* draws a calendar */
//function draw_calendar($month,$year){

$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 

"August", "September", "October", "November", "December");

if ($_REQUEST["month"]==0) $_REQUEST["month"] = date("n");

if ($_REQUEST["year"]==0) $_REQUEST["year"] = date("Y");


$month = $_REQUEST["month"];

$year = $_REQUEST["year"];



$prev_year = $year;

$next_year = $year;

$prev_month = $month-1;

$next_month = $month+1;



if ($prev_month == 0 ) {

	$prev_month = 12;

	$prev_year = $year - 1;

}

if ($next_month == 13 ) {

	$next_month = 1;

	$next_year = $year + 1;

}



$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();


	/* draw table */
	$sql = 'SELECT COUNT(*) AS day_bookings FROM bookings WHERE test_date ="'.date("Y-m-d",$list_date).'"';
			
			$bookings = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
	

$tmplEngine->assign(
        array(
        'monthNames'        		=>  $monthNames,
        'month'   			=>  $month,
		'year'   			=>  $year,
		'prev_year'   			=>  $prev_year,
		'next_year'   			=>  $next_year,
		'prev_month'   			=>  $prev_month,
		'next_month'   			=>  $next_month,
		'running_day'   			=>  $running_day,
		'days_in_month'   			=>  $days_in_month,
		'days_in_this_week'   			=>  $days_in_this_week,
		'day_counter'   			=>  $day_counter,
		'dates_array'   			=>  $dates_array,
		'bookings'   			=>  $bookings,
		'team'   			=>  $team,
        
        ));
?>