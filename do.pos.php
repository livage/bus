<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');


if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}


$fields = array(
	'STRING' => array(
		'name',
		'location',
		'owner',
						
	),
	'INT' => array(
	'capacity',
	),
	'FLOAT' => array(
	),
	'DATE' => array(
	
	),
	'DATETIME' => array(
	),
);

$mainTable = 'transaction';
$nextPage = 'index.php?c=pos';

switch ($op) {
    
    case 'I': // Insert
    
      $id = doOperation($connection, 'I', $mainTable, $fields, $_POST, $sqlError);
        
        break;
    case "U": // Update
    	echo $id;
        doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
		echo $sqlError;
        break;
      case 'D'://Status
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
       
   case 'S'://Status
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
    
}
?>
<script language="javascript" type="text/javascript">
   window.location = "<?php echo $nextPage?>";
</script>