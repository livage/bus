<?php /* Smarty version 2.6.10, created on 2013-01-17 07:57:09
         compiled from paper.htm */ ?>
<ol>
<!--multiple choice question -->
<h3>Multiple Choice Single Answer</h3>
	<?php unset($this->_sections['MCQ']);
$this->_sections['MCQ']['name'] = 'MCQ';
$this->_sections['MCQ']['loop'] = is_array($_loop=$this->_tpl_vars['MCQ']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['MCQ']['show'] = true;
$this->_sections['MCQ']['max'] = $this->_sections['MCQ']['loop'];
$this->_sections['MCQ']['step'] = 1;
$this->_sections['MCQ']['start'] = $this->_sections['MCQ']['step'] > 0 ? 0 : $this->_sections['MCQ']['loop']-1;
if ($this->_sections['MCQ']['show']) {
    $this->_sections['MCQ']['total'] = $this->_sections['MCQ']['loop'];
    if ($this->_sections['MCQ']['total'] == 0)
        $this->_sections['MCQ']['show'] = false;
} else
    $this->_sections['MCQ']['total'] = 0;
if ($this->_sections['MCQ']['show']):

            for ($this->_sections['MCQ']['index'] = $this->_sections['MCQ']['start'], $this->_sections['MCQ']['iteration'] = 1;
                 $this->_sections['MCQ']['iteration'] <= $this->_sections['MCQ']['total'];
                 $this->_sections['MCQ']['index'] += $this->_sections['MCQ']['step'], $this->_sections['MCQ']['iteration']++):
$this->_sections['MCQ']['rownum'] = $this->_sections['MCQ']['iteration'];
$this->_sections['MCQ']['index_prev'] = $this->_sections['MCQ']['index'] - $this->_sections['MCQ']['step'];
$this->_sections['MCQ']['index_next'] = $this->_sections['MCQ']['index'] + $this->_sections['MCQ']['step'];
$this->_sections['MCQ']['first']      = ($this->_sections['MCQ']['iteration'] == 1);
$this->_sections['MCQ']['last']       = ($this->_sections['MCQ']['iteration'] == $this->_sections['MCQ']['total']);
?>
	
<li><strong>Question : <?php echo $this->_tpl_vars['MCQ'][$this->_sections['MCQ']['index']]['question']; ?>
</Strong><br/></li>
<?php if ($this->_tpl_vars['MCQ'][$this->_sections['MCQ']['index']]['question_image'] == 1): ?>
<img src="images/questions_and_solutions/question<?php echo $this->_tpl_vars['MCQ'][$this->_sections['MCQ']['index']]['id']; ?>
.jpg"width="150px" height="150px"><br/>
<?php endif; ?>
<INPUT TYPE="hidden" NAME="question" VALUE="<?php echo $this->_tpl_vars['MCQ'][$this->_sections['MCQ']['index']]['id']; ?>
"/>
<?php unset($this->_sections['MCQsolution']);
$this->_sections['MCQsolution']['name'] = 'MCQsolution';
$this->_sections['MCQsolution']['loop'] = is_array($_loop=$this->_tpl_vars['MCQsolution'][$this->_sections['MCQ']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['MCQsolution']['show'] = true;
$this->_sections['MCQsolution']['max'] = $this->_sections['MCQsolution']['loop'];
$this->_sections['MCQsolution']['step'] = 1;
$this->_sections['MCQsolution']['start'] = $this->_sections['MCQsolution']['step'] > 0 ? 0 : $this->_sections['MCQsolution']['loop']-1;
if ($this->_sections['MCQsolution']['show']) {
    $this->_sections['MCQsolution']['total'] = $this->_sections['MCQsolution']['loop'];
    if ($this->_sections['MCQsolution']['total'] == 0)
        $this->_sections['MCQsolution']['show'] = false;
} else
    $this->_sections['MCQsolution']['total'] = 0;
if ($this->_sections['MCQsolution']['show']):

            for ($this->_sections['MCQsolution']['index'] = $this->_sections['MCQsolution']['start'], $this->_sections['MCQsolution']['iteration'] = 1;
                 $this->_sections['MCQsolution']['iteration'] <= $this->_sections['MCQsolution']['total'];
                 $this->_sections['MCQsolution']['index'] += $this->_sections['MCQsolution']['step'], $this->_sections['MCQsolution']['iteration']++):
$this->_sections['MCQsolution']['rownum'] = $this->_sections['MCQsolution']['iteration'];
$this->_sections['MCQsolution']['index_prev'] = $this->_sections['MCQsolution']['index'] - $this->_sections['MCQsolution']['step'];
$this->_sections['MCQsolution']['index_next'] = $this->_sections['MCQsolution']['index'] + $this->_sections['MCQsolution']['step'];
$this->_sections['MCQsolution']['first']      = ($this->_sections['MCQsolution']['iteration'] == 1);
$this->_sections['MCQsolution']['last']       = ($this->_sections['MCQsolution']['iteration'] == $this->_sections['MCQsolution']['total']);
?>
<INPUT TYPE="radio" NAME="answer<?php echo $this->_tpl_vars['MCQ'][$this->_sections['MCQ']['index']]['id']; ?>
" VALUE="<?php echo $this->_tpl_vars['MCQsolution'][$this->_sections['MCQ']['index']][$this->_sections['MCQsolution']['index']]['answer']; ?>
"><?php if ($this->_tpl_vars['MCQ'][$this->_sections['MCQ']['index']]['solutionImage'] == 1): ?><img src="images/questions_and_solutions/<?php echo $this->_tpl_vars['MCQsolution'][$this->_sections['MCQ']['index']][$this->_sections['MCQsolution']['index']]['answer']; ?>
"width="50px" height="50px"> <?php else:  echo $this->_tpl_vars['MCQsolution'][$this->_sections['MCQ']['index']][$this->_sections['MCQsolution']['index']]['answer'];  endif; ?><br/>
<?php endfor; endif; ?>
<?php endfor; endif; ?>

<!--multi response question -->
<h3>Multiple Choice Multiple Answer</h3>

	<?php unset($this->_sections['MCMQ']);
$this->_sections['MCMQ']['name'] = 'MCMQ';
$this->_sections['MCMQ']['loop'] = is_array($_loop=$this->_tpl_vars['MCMQ']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['MCMQ']['show'] = true;
$this->_sections['MCMQ']['max'] = $this->_sections['MCMQ']['loop'];
$this->_sections['MCMQ']['step'] = 1;
$this->_sections['MCMQ']['start'] = $this->_sections['MCMQ']['step'] > 0 ? 0 : $this->_sections['MCMQ']['loop']-1;
if ($this->_sections['MCMQ']['show']) {
    $this->_sections['MCMQ']['total'] = $this->_sections['MCMQ']['loop'];
    if ($this->_sections['MCMQ']['total'] == 0)
        $this->_sections['MCMQ']['show'] = false;
} else
    $this->_sections['MCMQ']['total'] = 0;
if ($this->_sections['MCMQ']['show']):

            for ($this->_sections['MCMQ']['index'] = $this->_sections['MCMQ']['start'], $this->_sections['MCMQ']['iteration'] = 1;
                 $this->_sections['MCMQ']['iteration'] <= $this->_sections['MCMQ']['total'];
                 $this->_sections['MCMQ']['index'] += $this->_sections['MCMQ']['step'], $this->_sections['MCMQ']['iteration']++):
$this->_sections['MCMQ']['rownum'] = $this->_sections['MCMQ']['iteration'];
$this->_sections['MCMQ']['index_prev'] = $this->_sections['MCMQ']['index'] - $this->_sections['MCMQ']['step'];
$this->_sections['MCMQ']['index_next'] = $this->_sections['MCMQ']['index'] + $this->_sections['MCMQ']['step'];
$this->_sections['MCMQ']['first']      = ($this->_sections['MCMQ']['iteration'] == 1);
$this->_sections['MCMQ']['last']       = ($this->_sections['MCMQ']['iteration'] == $this->_sections['MCMQ']['total']);
?>
	
<li><strong>Question : <?php echo $this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['question']; ?>
</Strong><br/></li>
<?php if ($this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['question_image'] == 1): ?>
<img src="images/questions_and_solutions/question<?php echo $this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['id']; ?>
.jpg"><br/>
<?php endif; ?>
<INPUT TYPE="hidden" NAME="question" VALUE="<?php echo $this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['id']; ?>
"/>
<?php unset($this->_sections['MCMQsolution']);
$this->_sections['MCMQsolution']['name'] = 'MCMQsolution';
$this->_sections['MCMQsolution']['loop'] = is_array($_loop=$this->_tpl_vars['MCMQsolution'][$this->_sections['MCMQ']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['MCMQsolution']['show'] = true;
$this->_sections['MCMQsolution']['max'] = $this->_sections['MCMQsolution']['loop'];
$this->_sections['MCMQsolution']['step'] = 1;
$this->_sections['MCMQsolution']['start'] = $this->_sections['MCMQsolution']['step'] > 0 ? 0 : $this->_sections['MCMQsolution']['loop']-1;
if ($this->_sections['MCMQsolution']['show']) {
    $this->_sections['MCMQsolution']['total'] = $this->_sections['MCMQsolution']['loop'];
    if ($this->_sections['MCMQsolution']['total'] == 0)
        $this->_sections['MCMQsolution']['show'] = false;
} else
    $this->_sections['MCMQsolution']['total'] = 0;
if ($this->_sections['MCMQsolution']['show']):

            for ($this->_sections['MCMQsolution']['index'] = $this->_sections['MCMQsolution']['start'], $this->_sections['MCMQsolution']['iteration'] = 1;
                 $this->_sections['MCMQsolution']['iteration'] <= $this->_sections['MCMQsolution']['total'];
                 $this->_sections['MCMQsolution']['index'] += $this->_sections['MCMQsolution']['step'], $this->_sections['MCMQsolution']['iteration']++):
$this->_sections['MCMQsolution']['rownum'] = $this->_sections['MCMQsolution']['iteration'];
$this->_sections['MCMQsolution']['index_prev'] = $this->_sections['MCMQsolution']['index'] - $this->_sections['MCMQsolution']['step'];
$this->_sections['MCMQsolution']['index_next'] = $this->_sections['MCMQsolution']['index'] + $this->_sections['MCMQsolution']['step'];
$this->_sections['MCMQsolution']['first']      = ($this->_sections['MCMQsolution']['iteration'] == 1);
$this->_sections['MCMQsolution']['last']       = ($this->_sections['MCMQsolution']['iteration'] == $this->_sections['MCMQsolution']['total']);
?>
<INPUT TYPE="checkbox" NAME="answer<?php echo $this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['id']; ?>
[]" VALUE="<?php echo $this->_tpl_vars['MCMQsolution'][$this->_sections['MCMQ']['index']][$this->_sections['MCMQsolution']['index']]['answer']; ?>
">
<?php if ($this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['solutionImage'] == 1): ?><img src="images/questions_and_solutions/<?php echo $this->_tpl_vars['MCMQsolution'][$this->_sections['MCMQ']['index']][$this->_sections['MCMQsolution']['index']]['answer']; ?>
" width="50px" height="50px"> <?php else:  echo $this->_tpl_vars['MCMQsolution'][$this->_sections['MCMQ']['index']][$this->_sections['MCMQsolution']['index']]['answer'];  endif; ?>
<br/>
<?php endfor; endif; ?>
<?php endfor; endif; ?>

<!--ordering quetstion -->
<h3>Ordering Answers</h3>
<?php unset($this->_sections['ORD']);
$this->_sections['ORD']['name'] = 'ORD';
$this->_sections['ORD']['loop'] = is_array($_loop=$this->_tpl_vars['ORD']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['ORD']['show'] = true;
$this->_sections['ORD']['max'] = $this->_sections['ORD']['loop'];
$this->_sections['ORD']['step'] = 1;
$this->_sections['ORD']['start'] = $this->_sections['ORD']['step'] > 0 ? 0 : $this->_sections['ORD']['loop']-1;
if ($this->_sections['ORD']['show']) {
    $this->_sections['ORD']['total'] = $this->_sections['ORD']['loop'];
    if ($this->_sections['ORD']['total'] == 0)
        $this->_sections['ORD']['show'] = false;
} else
    $this->_sections['ORD']['total'] = 0;
if ($this->_sections['ORD']['show']):

            for ($this->_sections['ORD']['index'] = $this->_sections['ORD']['start'], $this->_sections['ORD']['iteration'] = 1;
                 $this->_sections['ORD']['iteration'] <= $this->_sections['ORD']['total'];
                 $this->_sections['ORD']['index'] += $this->_sections['ORD']['step'], $this->_sections['ORD']['iteration']++):
$this->_sections['ORD']['rownum'] = $this->_sections['ORD']['iteration'];
$this->_sections['ORD']['index_prev'] = $this->_sections['ORD']['index'] - $this->_sections['ORD']['step'];
$this->_sections['ORD']['index_next'] = $this->_sections['ORD']['index'] + $this->_sections['ORD']['step'];
$this->_sections['ORD']['first']      = ($this->_sections['ORD']['iteration'] == 1);
$this->_sections['ORD']['last']       = ($this->_sections['ORD']['iteration'] == $this->_sections['ORD']['total']);
?>

<li><strong>Question : <?php echo $this->_tpl_vars['ORD'][$this->_sections['ORD']['index']]['question']; ?>
</Strong><br/></li>
<?php if ($this->_tpl_vars['ORD'][$this->_sections['ORD']['index']]['question_image'] == 1): ?>
<img src="images/questions_and_solutions/question<?php echo $this->_tpl_vars['ORD'][$this->_sections['ORD']['index']]['id']; ?>
.jpg"width="150px" height="150px"><br/>
<?php endif; ?>
<INPUT TYPE="hidden" NAME="question" VALUE="<?php echo $this->_tpl_vars['ORD'][$this->_sections['ORD']['index']]['id']; ?>
"/>
<?php unset($this->_sections['ORDsolution']);
$this->_sections['ORDsolution']['name'] = 'ORDsolution';
$this->_sections['ORDsolution']['loop'] = is_array($_loop=$this->_tpl_vars['ORDsolution'][$this->_sections['ORD']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['ORDsolution']['show'] = true;
$this->_sections['ORDsolution']['max'] = $this->_sections['ORDsolution']['loop'];
$this->_sections['ORDsolution']['step'] = 1;
$this->_sections['ORDsolution']['start'] = $this->_sections['ORDsolution']['step'] > 0 ? 0 : $this->_sections['ORDsolution']['loop']-1;
if ($this->_sections['ORDsolution']['show']) {
    $this->_sections['ORDsolution']['total'] = $this->_sections['ORDsolution']['loop'];
    if ($this->_sections['ORDsolution']['total'] == 0)
        $this->_sections['ORDsolution']['show'] = false;
} else
    $this->_sections['ORDsolution']['total'] = 0;
if ($this->_sections['ORDsolution']['show']):

            for ($this->_sections['ORDsolution']['index'] = $this->_sections['ORDsolution']['start'], $this->_sections['ORDsolution']['iteration'] = 1;
                 $this->_sections['ORDsolution']['iteration'] <= $this->_sections['ORDsolution']['total'];
                 $this->_sections['ORDsolution']['index'] += $this->_sections['ORDsolution']['step'], $this->_sections['ORDsolution']['iteration']++):
$this->_sections['ORDsolution']['rownum'] = $this->_sections['ORDsolution']['iteration'];
$this->_sections['ORDsolution']['index_prev'] = $this->_sections['ORDsolution']['index'] - $this->_sections['ORDsolution']['step'];
$this->_sections['ORDsolution']['index_next'] = $this->_sections['ORDsolution']['index'] + $this->_sections['ORDsolution']['step'];
$this->_sections['ORDsolution']['first']      = ($this->_sections['ORDsolution']['iteration'] == 1);
$this->_sections['ORDsolution']['last']       = ($this->_sections['ORDsolution']['iteration'] == $this->_sections['ORDsolution']['total']);
?>
<select  NAME="answer<?php echo $this->_tpl_vars['ORD'][$this->_sections['ORD']['index']]['id']; ?>
[]">
<option VALUE="">----</option>
<?php unset($this->_sections['ORDsolution1']);
$this->_sections['ORDsolution1']['name'] = 'ORDsolution1';
$this->_sections['ORDsolution1']['loop'] = is_array($_loop=$this->_tpl_vars['ORDsolution'][$this->_sections['ORD']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['ORDsolution1']['show'] = true;
$this->_sections['ORDsolution1']['max'] = $this->_sections['ORDsolution1']['loop'];
$this->_sections['ORDsolution1']['step'] = 1;
$this->_sections['ORDsolution1']['start'] = $this->_sections['ORDsolution1']['step'] > 0 ? 0 : $this->_sections['ORDsolution1']['loop']-1;
if ($this->_sections['ORDsolution1']['show']) {
    $this->_sections['ORDsolution1']['total'] = $this->_sections['ORDsolution1']['loop'];
    if ($this->_sections['ORDsolution1']['total'] == 0)
        $this->_sections['ORDsolution1']['show'] = false;
} else
    $this->_sections['ORDsolution1']['total'] = 0;
if ($this->_sections['ORDsolution1']['show']):

            for ($this->_sections['ORDsolution1']['index'] = $this->_sections['ORDsolution1']['start'], $this->_sections['ORDsolution1']['iteration'] = 1;
                 $this->_sections['ORDsolution1']['iteration'] <= $this->_sections['ORDsolution1']['total'];
                 $this->_sections['ORDsolution1']['index'] += $this->_sections['ORDsolution1']['step'], $this->_sections['ORDsolution1']['iteration']++):
$this->_sections['ORDsolution1']['rownum'] = $this->_sections['ORDsolution1']['iteration'];
$this->_sections['ORDsolution1']['index_prev'] = $this->_sections['ORDsolution1']['index'] - $this->_sections['ORDsolution1']['step'];
$this->_sections['ORDsolution1']['index_next'] = $this->_sections['ORDsolution1']['index'] + $this->_sections['ORDsolution1']['step'];
$this->_sections['ORDsolution1']['first']      = ($this->_sections['ORDsolution1']['iteration'] == 1);
$this->_sections['ORDsolution1']['last']       = ($this->_sections['ORDsolution1']['iteration'] == $this->_sections['ORDsolution1']['total']);
?>

<option VALUE="<?php echo $this->_tpl_vars['ORDsolution'][$this->_sections['ORD']['index']][$this->_sections['ORDsolution1']['index']]['answer']; ?>
"><?php echo $this->_tpl_vars['ORDsolution'][$this->_sections['ORD']['index']][$this->_sections['ORDsolution1']['index']]['answer']; ?>
</option>
<?php endfor; endif; ?>
</select>
<?php endfor; endif; ?>
<?php endfor; endif; ?>


<!--free response question -->
<h3>Free Answer questions</h3>


<?php unset($this->_sections['FRQ']);
$this->_sections['FRQ']['name'] = 'FRQ';
$this->_sections['FRQ']['loop'] = is_array($_loop=$this->_tpl_vars['FRQ']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['FRQ']['show'] = true;
$this->_sections['FRQ']['max'] = $this->_sections['FRQ']['loop'];
$this->_sections['FRQ']['step'] = 1;
$this->_sections['FRQ']['start'] = $this->_sections['FRQ']['step'] > 0 ? 0 : $this->_sections['FRQ']['loop']-1;
if ($this->_sections['FRQ']['show']) {
    $this->_sections['FRQ']['total'] = $this->_sections['FRQ']['loop'];
    if ($this->_sections['FRQ']['total'] == 0)
        $this->_sections['FRQ']['show'] = false;
} else
    $this->_sections['FRQ']['total'] = 0;
if ($this->_sections['FRQ']['show']):

            for ($this->_sections['FRQ']['index'] = $this->_sections['FRQ']['start'], $this->_sections['FRQ']['iteration'] = 1;
                 $this->_sections['FRQ']['iteration'] <= $this->_sections['FRQ']['total'];
                 $this->_sections['FRQ']['index'] += $this->_sections['FRQ']['step'], $this->_sections['FRQ']['iteration']++):
$this->_sections['FRQ']['rownum'] = $this->_sections['FRQ']['iteration'];
$this->_sections['FRQ']['index_prev'] = $this->_sections['FRQ']['index'] - $this->_sections['FRQ']['step'];
$this->_sections['FRQ']['index_next'] = $this->_sections['FRQ']['index'] + $this->_sections['FRQ']['step'];
$this->_sections['FRQ']['first']      = ($this->_sections['FRQ']['iteration'] == 1);
$this->_sections['FRQ']['last']       = ($this->_sections['FRQ']['iteration'] == $this->_sections['FRQ']['total']);
?>

<li><strong>Question : <?php echo $this->_tpl_vars['FRQ'][$this->_sections['FRQ']['index']]['question']; ?>
</Strong></li>
<?php if ($this->_tpl_vars['FRQ'][$this->_sections['FRQ']['index']]['question_image'] == 1): ?>
<img src="images/questions_and_solutions/question<?php echo $this->_tpl_vars['FRQ'][$this->_sections['FRQ']['index']]['id']; ?>
.jpg"width="150px" height="150px"><br/>
<?php endif; ?>
<INPUT TYPE="hidden" NAME="question" VALUE="<?php echo $this->_tpl_vars['FRQ'][$this->_sections['FRQ']['index']]['id']; ?>
"/>
<textarea name="solution1" size=50></textarea> <br/>

<?php endfor; endif; ?>

<!--true or false response question -->
<h3>True or Fasle questions</h3>


<?php unset($this->_sections['TFQ']);
$this->_sections['TFQ']['name'] = 'TFQ';
$this->_sections['TFQ']['loop'] = is_array($_loop=$this->_tpl_vars['TFQ']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['TFQ']['show'] = true;
$this->_sections['TFQ']['max'] = $this->_sections['TFQ']['loop'];
$this->_sections['TFQ']['step'] = 1;
$this->_sections['TFQ']['start'] = $this->_sections['TFQ']['step'] > 0 ? 0 : $this->_sections['TFQ']['loop']-1;
if ($this->_sections['TFQ']['show']) {
    $this->_sections['TFQ']['total'] = $this->_sections['TFQ']['loop'];
    if ($this->_sections['TFQ']['total'] == 0)
        $this->_sections['TFQ']['show'] = false;
} else
    $this->_sections['TFQ']['total'] = 0;
if ($this->_sections['TFQ']['show']):

            for ($this->_sections['TFQ']['index'] = $this->_sections['TFQ']['start'], $this->_sections['TFQ']['iteration'] = 1;
                 $this->_sections['TFQ']['iteration'] <= $this->_sections['TFQ']['total'];
                 $this->_sections['TFQ']['index'] += $this->_sections['TFQ']['step'], $this->_sections['TFQ']['iteration']++):
$this->_sections['TFQ']['rownum'] = $this->_sections['TFQ']['iteration'];
$this->_sections['TFQ']['index_prev'] = $this->_sections['TFQ']['index'] - $this->_sections['TFQ']['step'];
$this->_sections['TFQ']['index_next'] = $this->_sections['TFQ']['index'] + $this->_sections['TFQ']['step'];
$this->_sections['TFQ']['first']      = ($this->_sections['TFQ']['iteration'] == 1);
$this->_sections['TFQ']['last']       = ($this->_sections['TFQ']['iteration'] == $this->_sections['TFQ']['total']);
?>

<li><strong>Question : <?php echo $this->_tpl_vars['TFQ'][$this->_sections['TFQ']['index']]['question']; ?>
</Strong></li>
<?php if ($this->_tpl_vars['TFQ'][$this->_sections['TFQ']['index']]['question_image'] == 1): ?>
<img src="images/questions_and_solutions/question<?php echo $this->_tpl_vars['TFQ'][$this->_sections['TFQ']['index']]['id']; ?>
.jpg"width="150px" height="150px"><br/>
<?php endif; ?>
<INPUT TYPE="hidden" NAME="question" VALUE="<?php echo $this->_tpl_vars['TFQ'][$this->_sections['TFQ']['index']]['id']; ?>
"/>
<INPUT TYPE="checkbox" NAME="answer<?php echo $this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['id']; ?>
[]" VALUE="T"> True
<INPUT TYPE="checkbox" NAME="answer<?php echo $this->_tpl_vars['MCMQ'][$this->_sections['MCMQ']['index']]['id']; ?>
[]" VALUE="F"> False
<?php endfor; endif; ?>
</ol>
