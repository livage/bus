<?php /* Smarty version 2.6.10, created on 2015-12-04 15:42:59
         compiled from sale.htm */ ?>
<!--<div id="main">
<div id="content">
<div id="top_con"><div id="page_title"> Ticket Sale ( <?php echo $this->_tpl_vars['match'][0]['t1name']; ?>
 vs <?php echo $this->_tpl_vars['match'][0]['t2name']; ?>
)</div>
<a href="#">Table</a>
</div>
<form action="do.sale.php?id=<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['match_id']; ?>
" method="post" enctype="multipart/form-data">
   <input type="hidden" name="op"  value="<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
" />
<table class="table">
	<thead class="header_table">
				<th  width="2%"></th>
				<th  width="2%">Ticket Type </th>
				<th  width="64%">Match</th>
				<th  width="8%">Price</th>
				
				
			
	</thead>
<tbody>
<?php echo $this->_tpl_vars['count']++; ?>

<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['match']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>
<td><input type='radio' name='id' value="<?php echo $this->_tpl_vars['count']++; ?>
"/></td>
	<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['name']; ?>
<input type='hidden' name='section[]' value="<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['id']; ?>
"/></td>
	<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['t1name']; ?>
 vs <?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['t2name']; ?>
 <?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['match_date']; ?>
 <?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['match_time']; ?>
<input type='hidden' name='match_id' value="<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['match_id']; ?>
"/></td>
	<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['price']; ?>
<input type='hidden' name='amount[]' value="<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['price']; ?>
"/></td>
</tr>

<?php endfor; endif; ?>
</tbody>

</table>
		<input type="submit" value="Confirm" class="serviceSub row_btn"/>
	    <input type="button" value="Cancel" class="serviceCan row_bt" href="pos.php" />
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div></div>-->



<form action="index.php?c=list" method="post" enctype="multipart/form-data">
<div class="clearfix topPart">	

		<h2 >Online Bus Tickets Booking </h2>
		<div class="searchRow clearfix">
			<div class="LB">
				<label class="inputLabel">From</label>
				<input type="text" data-message="Please enter a source city|Invalid City" data-validate="required|validSource" tabindex="1" placeholder="Enter a city" class="XXinput" autocomplete="off" id="txtSource">
				<input type="hidden" name="stationA"  id="Source">
				<div class="errorMessageFixed"> </div>
			</div>
			
			<div class="searchRight retJouney">
				<label class="inputLabel">To</label>
				<input type="text" data-message="Please enter a destination city|Invalid City" data-validate="required|validDestination" tabindex="2" placeholder="Enter a city" class="XXinput" autocomplete="off" id="txtDestination">
				<input type="hidden" name="stationB"  id="Destination">
				<div class="errorMessageFixed"> </div>               
			</div>
		</div>
		<div class="searchRow clearfix">
			<div class="LB">
				<label class="inputLabel">Date of Journey</label>
				<span class="blackTextSmall"></span>
				<input type="text" readonly="readonly" tabindex="3" title="Date in the format dd-mmm-yyyy" placeholder="dd-mmm-yyyy" class="XXinput calendar" id="txtOnwardCalendar">
			</div>               
			<div class="searchRight retJouney">
				<label class="inputLabel">Date of Return<span class="opt">&nbsp;(Optional)</span></label>
				<input type="text" readonly="readonly" tabindex="4" title="Date in the format dd-mmm-yyyy" placeholder="dd-mmm-yyyy" class="XXinput calendar" id="txtReturnCalendar">
			</div>    
		</div> 

		<div class="dateError">Onward date should be before return date</div>
		<div class="buttonContainer">
			<button tabindex="6" id="searchBtn" class="RB Xbutton">Search Buses</button>
		</div>


</div>
</form>