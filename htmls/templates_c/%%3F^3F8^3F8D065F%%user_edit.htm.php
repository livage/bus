<?php /* Smarty version 2.6.10, created on 2015-12-14 10:47:19
         compiled from user_edit.htm */ ?>
<form action="do.user.php?id=<?php echo $this->_tpl_vars['users'][0]['id']; ?>
" method="post" enctype="multipart/form-data">
	<div id="content">
	
        <fieldset >
		
			<legend>Users</legend>
			
	        <input type="hidden" name="op"  value="<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
" />
			
			<div class="boxfield">
			 	<label for="name">Name:</label> <input type="text" name="name" size="70" value="<?php echo $this->_tpl_vars['users'][0]['name']; ?>
" required/>
				
			 </div>
			 
			<div class="boxfield">
				<label for="username">Username:</label><input type="text" name="username" size="70" value="<?php echo $this->_tpl_vars['users'][0]['username']; ?>
" required/> 
			</div>
		<?php if ($this->_tpl_vars['_ENGINE']['operation'] == I): ?>
			<div  class="boxfield">
				<label for="user_password">Password:</label> <input type="password" name="user_password" size="70" value="<?php echo $this->_tpl_vars['users'][0]['user_password']; ?>
" required/>
			</div>
			  <?php endif; ?>
			<div class="boxfield">
				<label for="email">Email:</label> <input type="text" name="email" size="70" value="<?php echo $this->_tpl_vars['users'][0]['email']; ?>
" required/>
			</div>
			  
			<div  class="boxfield">
				<label for="profile">Profile:</label> <select  name="profile" >
				<option >Select a profile</option>
				<option value="A" <?php if ($this->_tpl_vars['users'][0]['profile'] == A): ?> selected <?php endif; ?>>Administrator</option>
				<option value="T" <?php if ($this->_tpl_vars['users'][0]['profile'] == T): ?> <?php if ($this->_tpl_vars['users'][0]['tv'] == T): ?>selected <?php endif; ?> <?php endif; ?>>Team Administrator</option>
				<option value="T" <?php if ($this->_tpl_vars['users'][0]['profile'] == T): ?> <?php if ($this->_tpl_vars['users'][0]['tv'] == V): ?>selected <?php endif;  endif; ?>>Venue Administrator</option>
				<option value="P" <?php if ($this->_tpl_vars['users'][0]['profile'] == P): ?> selected <?php endif; ?>>Teller</option>
				<option value="G" <?php if ($this->_tpl_vars['users'][0]['profile'] == G): ?> selected <?php endif; ?>>Gate </option>
				</select>
			</div>
			
	<!--	<div class="boxfield">
		
				<input type="radio" id="chkbox" name="profile" <?php if ($this->_tpl_vars['users'][0]['tv'] == T): ?> checked<?php endif; ?>>Team
				<input type="radio" id="chkbox" name="profile" <?php if ($this->_tpl_vars['users'][0]['tv'] == V): ?> checked<?php endif; ?>>Venue
				
			 </div>
			 
			<div id="image"  style="display:<?php if ($this->_tpl_vars['users'][0]['tv'] == T): ?> block<?php else: ?> none<?php endif; ?>;" style=" visibility: hidden;" class="boxfield">
			<div  class="boxfield">
				<label for="team1">Team :</label> <select  NAME="team1" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['teams']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>

					<?php if ($this->_tpl_vars['teams'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['users'][0]['tvid']): ?>
<option VALUE="<?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['teams'][$this->_sections['list']['index']]['name']; ?>
</option>
<?php else: ?>

					<option VALUE="<?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['teams'][$this->_sections['list']['index']]['name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>
			</div>
			
			<img src="images/logos/<?php echo $this->_tpl_vars['team'][0]['id']; ?>
logo.jpg" alt="logo" height="42" width="42"> 
				<label for="team_image">Image:</label><input type="file" name="team_image" size="70"> 
				
			</div>
			
			
			<div id="image"  style="display:<?php if ($this->_tpl_vars['users'][0]['tv'] == V): ?> block<?php else: ?> none<?php endif; ?>;" style=" visibility: hidden;" class="boxfield">
			<div class="boxfield">
				<label for="venue">Venue:</label>
				
				<select  NAME="venue" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['venue']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<?php if ($this->_tpl_vars['venue'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['match'][0]['venue']): ?>
<option VALUE="<?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['name']; ?>
</option>
<?php else: ?>
					<option VALUE="<?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>

			</div>
			-->
				
			</div>
        </fieldset>
	   
		<input type="submit" value="Confirm" class="serviceSub row_btn"/>
	    <input type="button" value="Cancel" class="serviceCan row_bt" href="question_list.php" />
	</div>
</form>
