<?php /* Smarty version 2.6.10, created on 2013-01-18 13:57:11
         compiled from update.htm */ ?>
<script type="text/javascript" src="js/hide.js" ></script>
<div id="formpage_1" name="insert" style="visibility: visible; display: block; .."> 
  
 <form action="do.update.php?id=<?php echo $this->_tpl_vars['question'][0]['id']; ?>
" method="post" enctype="multipart/form-data">

	  <div id="content">
        <fieldset class="manufacturer">
		<legend><?php echo $this->_tpl_vars['question'][0]['section']; ?>
</legend>
	        
			<div class="boxfield">
			 	Question:
			 <input type="text" name="question" size="70" value ="<?php echo $this->_tpl_vars['question'][0]['question']; ?>
"> 
			 </div>
			 <?php if ($this->_tpl_vars['question'][0]['questionImage'] == 1): ?>
		<div id="image"   class="boxfield">
		Question Image:
		<img src="images/questions_and_solutions/question<?php echo $this->_tpl_vars['question'][0]['id']; ?>
.jpg"width="150px" height="150px"><br/>
		<input type="file" name="question_image" size="70"> 
		</div>
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['question'][0]['section'] == TFQ): ?>
		<input type="radio" name="correctAns" value="true" <?php if ($this->_tpl_vars['correct'][0]['answer'] == true): ?>checked="checked"<?php endif; ?>/>True 
		<input type="radio" name="correctAns" value="false" <?php if ($this->_tpl_vars['correct'][0]['answer'] == false): ?>checked="checked"<?php endif; ?>/>False
		<?php elseif ($this->_tpl_vars['question'][0]['section'] == FRQ): ?>
		
		<div class="boxfield">
			 	<textarea name="correctAns" size=50><?php echo $this->_tpl_vars['correct'][0]['answer']; ?>
</textarea> 
			 </div>
		<?php else: ?>
			<br style="clear:both" />
			<h4>	Correct Solutions:</h4>
			 <?php unset($this->_sections['correct']);
$this->_sections['correct']['name'] = 'correct';
$this->_sections['correct']['loop'] = is_array($_loop=$this->_tpl_vars['correct']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['correct']['show'] = true;
$this->_sections['correct']['max'] = $this->_sections['correct']['loop'];
$this->_sections['correct']['step'] = 1;
$this->_sections['correct']['start'] = $this->_sections['correct']['step'] > 0 ? 0 : $this->_sections['correct']['loop']-1;
if ($this->_sections['correct']['show']) {
    $this->_sections['correct']['total'] = $this->_sections['correct']['loop'];
    if ($this->_sections['correct']['total'] == 0)
        $this->_sections['correct']['show'] = false;
} else
    $this->_sections['correct']['total'] = 0;
if ($this->_sections['correct']['show']):

            for ($this->_sections['correct']['index'] = $this->_sections['correct']['start'], $this->_sections['correct']['iteration'] = 1;
                 $this->_sections['correct']['iteration'] <= $this->_sections['correct']['total'];
                 $this->_sections['correct']['index'] += $this->_sections['correct']['step'], $this->_sections['correct']['iteration']++):
$this->_sections['correct']['rownum'] = $this->_sections['correct']['iteration'];
$this->_sections['correct']['index_prev'] = $this->_sections['correct']['index'] - $this->_sections['correct']['step'];
$this->_sections['correct']['index_next'] = $this->_sections['correct']['index'] + $this->_sections['correct']['step'];
$this->_sections['correct']['first']      = ($this->_sections['correct']['iteration'] == 1);
$this->_sections['correct']['last']       = ($this->_sections['correct']['iteration'] == $this->_sections['correct']['total']);
?>
			  <div id="textsol"  class="boxfield">
			  Correct Answer: <?php if ($this->_tpl_vars['correct'][$this->_sections['correct']['index']]['image'] == 1): ?>
			  <img src="images/questions_and_solutions/<?php echo $this->_tpl_vars['correct'][$this->_sections['correct']['index']]['answer']; ?>
"width="150px" height="150px"><br/>
			  <input type="file" name="correctAns[]" size=70/> 
			  <?php else: ?><input type="text" name="correctAns[]" size="70" value='<?php echo $this->_tpl_vars['correct'][$this->_sections['correct']['index']]['answer']; ?>
'/><?php endif; ?>
			  </div>
			  
			 <?php endfor; endif; ?>
			
			   <br style="clear:both" />
			  <div class="boxfield">
			 
			 <div id="misleading">
			<h4>	Misleading Solutions:</h4>
			 	
				 <br style="clear:both" />
			<?php unset($this->_sections['misledding']);
$this->_sections['misledding']['name'] = 'misledding';
$this->_sections['misledding']['loop'] = is_array($_loop=$this->_tpl_vars['misledding']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['misledding']['show'] = true;
$this->_sections['misledding']['max'] = $this->_sections['misledding']['loop'];
$this->_sections['misledding']['step'] = 1;
$this->_sections['misledding']['start'] = $this->_sections['misledding']['step'] > 0 ? 0 : $this->_sections['misledding']['loop']-1;
if ($this->_sections['misledding']['show']) {
    $this->_sections['misledding']['total'] = $this->_sections['misledding']['loop'];
    if ($this->_sections['misledding']['total'] == 0)
        $this->_sections['misledding']['show'] = false;
} else
    $this->_sections['misledding']['total'] = 0;
if ($this->_sections['misledding']['show']):

            for ($this->_sections['misledding']['index'] = $this->_sections['misledding']['start'], $this->_sections['misledding']['iteration'] = 1;
                 $this->_sections['misledding']['iteration'] <= $this->_sections['misledding']['total'];
                 $this->_sections['misledding']['index'] += $this->_sections['misledding']['step'], $this->_sections['misledding']['iteration']++):
$this->_sections['misledding']['rownum'] = $this->_sections['misledding']['iteration'];
$this->_sections['misledding']['index_prev'] = $this->_sections['misledding']['index'] - $this->_sections['misledding']['step'];
$this->_sections['misledding']['index_next'] = $this->_sections['misledding']['index'] + $this->_sections['misledding']['step'];
$this->_sections['misledding']['first']      = ($this->_sections['misledding']['iteration'] == 1);
$this->_sections['misledding']['last']       = ($this->_sections['misledding']['iteration'] == $this->_sections['misledding']['total']);
?>
			  <div id="textsol"  class="boxfield">
			 Wrong Answer: <?php if ($this->_tpl_vars['misledding'][$this->_sections['misledding']['index']]['image'] == 1): ?>
			  <img src="images/questions_and_solutions/<?php echo $this->_tpl_vars['misledding'][$this->_sections['misledding']['index']]['answer']; ?>
"width="150px" height="150px"><br/>
			  <input type="file" name="answer[]" size=70/> 
			  <?php else: ?><input type="text" name="answer[]" size="70" value="<?php echo $this->_tpl_vars['misledding'][$this->_sections['misledding']['index']]['answer']; ?>
"/><?php endif; ?>
			  </div>
			  
			 <?php endfor; endif; ?>
			  </div>
			 </div>
			 <br style="clear:both" />
			
			 <?php endif; ?>
        </fieldset>
	    <input type="submit" value="Update" class="serviceSub row_btn"/>
	    <input type="button" value="Cancel" class="serviceCan row_bt" href="index.php?c=question_management" />
	 </div>
</form>
<input type="type">
</div>