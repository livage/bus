<?php /* Smarty version 2.6.10, created on 2011-01-14 14:43:46
         compiled from s_news_management-edit.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 's_news_management-edit.htm', 25, false),)), $this); ?>
<p class="pageTitle">News Management (<?php echo $this->_tpl_vars['opString']; ?>
)</p>

<form action="do.newsManagement.php?op=<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
&id=<?php echo $this->_tpl_vars['new']['id']; ?>
" method="post"  enctype="multipart/form-data">
	
	<div class="break"></div>
	<div class="boxfield">
	    <label>Date</label>
	    <input type="text" name="date" class="_fDate" value="<?php echo $this->_tpl_vars['new']['date']; ?>
" />
	</div>

	<div class="break"></div>

	
	<div class="boxfield">
	    <label>Title</label>
	   <input type="text"  style="width: 255px"  name="title" id="title" value="<?php echo $this->_tpl_vars['new']['title']; ?>
" />
	 
	</div>
	<div class="break"></div>
	
	<div class="boxfield">
	    <label>Country</label>
	    <select name="country">
	    	<option value="">All</value>
	    	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lists']['country'],'selected' => $this->_tpl_vars['new']['country']), $this);?>

	    </select>
	</div>
	<div class="break"></div>

	<div class="boxfield">
	    <label>Text</label>
	    <textarea name="text" style="width: 460px" id="text"><?php echo $this->_tpl_vars['new']['text']; ?>
</textarea>
	</div>
	
	<div class="break"></div>
		
	<div class="boxfield">
	    <label>Video Link</label>   
			 <input type="text" style="width: 460px" name="video" value ="<?php echo $this->_tpl_vars['new']['video']; ?>
"  maxlength="255"/>        
	</div>

	

	<div class="break"></div>
	<?php if ($this->_tpl_vars['_ENGINE']['operation'] == 'I'): ?>

	<div class="boxfield">
	    <label>New Picture</label>  
		<input type="file" name="image" id="image" value="$new.image"/>
	</div>
	<?php else: ?>
	<div class="boxfield">
		Current Picture<br />
		<div class="immagine_presente">
		 <img src="<?php echo $this->_tpl_vars['new']['photoSmall']; ?>
">
		  <!--<a href="javascript:unlinkImage(<?php echo $this->_tpl_vars['new']['id']; ?>
)">Remove Picture</a>-->
		  <a href="do.removeFile.php?type=image&id=<?php echo $this->_tpl_vars['new']['id']; ?>
">Rimuovi</a>
		</div>
	</div>
	<div class="break"></div>
	<div class="boxfield">
	    <label>New Picture</label>  
		<input type="file" name="image" id="image" value="$new.image"/>
	</div>
	<?php endif; ?>

	
	<div class="break"></div>

	<input type="button" value="Back" onclick="window.location='index.php?c=news-management'"/>

		<input type="submit" value="Save" />
</form>