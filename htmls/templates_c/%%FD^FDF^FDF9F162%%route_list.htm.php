<?php /* Smarty version 2.6.10, created on 2015-12-14 11:19:48
         compiled from route_list.htm */ ?>
<div id="main">
<div id="content">

<table class="table">
	<thead class="header_table">
				<th  width="5%">Route ID</th>
				<th  width="10%">Code</th>
				<th  width="60%">Route</th>
				<th  width="10%">Cost</th>
				<th  width="15%">Action</th>
			
	</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['route']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>

	<td><?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
</td>
	<td><?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['code']; ?>
</td>
	<td><?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['sa_name']; ?>
 to <?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['sb_name']; ?>
</td>
	<td><?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['cost']; ?>
</td>
	<td>
		<a href="do.route.php?op=D&id=<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
" onclick="return confirm('Are you sure you want to delete this record? ');"/><img src="images/icons/delete.png" /></a>
		<a href="index.php?c=route_edit&id=<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/edit.png" /></a>
		<a href="do.route.php?op=S&id=<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
" ><img src="images/icons/status<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['active']; ?>
.png" />
		</a>
	</td>
</tr>
<?php endfor; endif; ?>
</tbody>

</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div></div>