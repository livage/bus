<?php /* Smarty version 2.6.10, created on 2011-01-14 14:58:55
         compiled from u_search_friends.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'u_search_friends.htm', 14, false),)), $this); ?>
<p class="pageTitle">search for friends</p>

<div class="tableFilter">
<form action="index.php?c=search_friends" method="post">
	
	<div class="boxfield">
	    <label>name/surname:</label>
		<input type="text" value="" name="sur_name">
	</div>
	<div class="boxfield">
          	<label >Gender</label>
			<select name="gender" >
			<option></option>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['gender'],'selected' => $this->_tpl_vars['gender']['gender']), $this);?>

			</select>
            	</div>
			<div class="boxfield">
	          	<label >Marital status</label>
				<select  name="status" >
			<option></option>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['status'],'selected' => $this->_tpl_vars['status']['status']), $this);?>

			</select>
    </div>
	<div class="boxfield">
	   <label>city:</label>
	   <input type="text" value="" name="city">
	</div>
	
	<div class="boxfield">
	    <label>country:</label>
		<select  name="country" >
			<option></option>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['cons'],'selected' => $this->_tpl_vars['cons']['country']), $this);?>

			</select>
	</div>
	<div class="boxfield">
	    <label>Age From</label>
	    <input type="text" name="age_from"  value="<?php echo $this->_tpl_vars['filter']['from']; ?>
" />
	</div>
	<div class="boxfield">
	    <label>Age To</label>
	    <input type="text" name="age_to"  value="<?php echo $this->_tpl_vars['filter']['to']; ?>
" />
	</div>
	<div class="break"></div>
	<input type="submit" value="button" name="button" />
</form>
</div>

<div id="wBBNews">
	<div id="title">Friends List</div>
	<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['sfriend']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
	<div class="newsItem">
	
		
		<div class="newsFrom">
			
			<img src="<?php echo $this->_tpl_vars['sfriend'][$this->_sections['list']['index']]['photoSmall']; ?>
">
		</div>
		<div class="break"></div>
		<div class="newsText">
		<a href="index.php?c=searched_friend&id=<?php echo $this->_tpl_vars['sfriend'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sfriend'][$this->_sections['list']['index']]['name']; ?>
</a>
		</div>
		<div class="break"></div>
	

	</div>

	<?php endfor; endif; ?>
	
</div>
