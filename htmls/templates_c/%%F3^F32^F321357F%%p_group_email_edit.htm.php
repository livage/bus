<?php /* Smarty version 2.6.10, created on 2011-01-14 07:22:15
         compiled from p_group_email_edit.htm */ ?>
<p class="pageTitle">Group E-Mails (<?php echo $this->_tpl_vars['opString']; ?>
)</p>

<form action="do.groupemails.php?op=<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
&id=<?php echo $this->_tpl_vars['email']['id']; ?>
" method="post" id="massiveEmail">

	<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['email']['id']; ?>
" />

	<div class="boxfield">
	    <label>Subject</label>
	    <input type="text" name="subject" id="subject" maxlength="255" value="<?php echo $this->_tpl_vars['email']['subject']; ?>
" />
	</div>

	<div class="break"></div>

	<div class="boxfield">
	    <label>Text</label>
	    <textarea name="message" id="message"><?php echo $this->_tpl_vars['email']['message']; ?>
</textarea>
	</div>

	<div class="break"></div>

	<fieldset>
		<legend>Send a test</legend>

		<div class="boxfield">
		    <label>Test E-Mail</label>
		    <input type="text" name="testEmail" id="testEmail" maxlength="255" value="<?php echo $this->_tpl_vars['email']['testEmail']; ?>
" />
		</div>

		<div class="break"></div>

		<input type="button" value="Send a TEST" onclick="sendMessage('test')" />
	</fieldset>

	<input type="button" value="Send the message to ALL" onclick="sendMessage('final')" />

	<input type="submit" value="Save">

</form>