<?php /* Smarty version 2.6.10, created on 2015-07-08 11:12:07
         compiled from template/admin.htm */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Bus Stop</title>
<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon'/ >

<link href="css/public.css" rel="stylesheet" type="text/css" ></link>
<link rel="stylesheet" type="text/css" href="css/layout.css"></link>
<link rel="stylesheet" type="text/css" href="css/menu.css"></link>
<link rel="stylesheet" type="text/css" href="css/buttons-divs-pag.css"></link>
<link rel="stylesheet" type="text/css" href="css/form.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="css/core.css"></link>
<script type="text/javascript" src="libs/jquery/jquery.js"></script>
<!--<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>-->
<script type="text/javascript" src="libs/jquery/jquery.maskedinput.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/initPage.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/hide.js" ></script>

<?php if ($this->_tpl_vars['_ENGINE']['contentClass']): ?>
<link rel="stylesheet" type="text/css" href="css/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.css"></link>
<script type="text/javascript" src="js/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.js"></script>
<?php endif; ?>

<link rel="stylesheet" type="text/css" href="datetimepicker/jquery.datetimepicker.css"/>
<script src="datetimepicker/jquery.js"></script>
<script src="datetimepicker/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="js/picker.js" ></script>
</head>
<body>

<div id="header"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_header.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>

<div id="container">
	
	    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['page_content'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	
</div>
<div class="clear"></div>
<div id="footer">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>

</body>

</html>