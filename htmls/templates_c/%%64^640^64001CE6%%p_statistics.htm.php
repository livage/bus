<?php /* Smarty version 2.6.10, created on 2011-01-14 07:21:36
         compiled from p_statistics.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'p_statistics.htm', 19, false),)), $this); ?>
<p class="pageTitle">Statistics</p>

<div class="tableFilter">
<form action="index.php?c=statistics" method="post">
	<div class="boxfield">
	    <label>From Date</label>
	    <input type="text" name="dateFrom" class="_fDate" value="<?php echo $this->_tpl_vars['filter']['dateFrom']; ?>
" />
	</div>

	<div class="boxfield">
	    <label>To Date</label>
	    <input type="text" name="dateTo" class="_fDate" value="<?php echo $this->_tpl_vars['filter']['dateTo']; ?>
" />
	</div>

	<div class="boxfield">
	    <label>Sex</label>
	    <select name="gender">
	    	<option value="">Both</option>
	    	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lists']['gender'],'selected' => $this->_tpl_vars['filter']['gender']), $this);?>

	    </select>
	</div>

	<div class="boxfield">
	    <label>Continent</label>
	    <select name="continent">
	    	<option value="">All</value>
	    	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lists']['continent'],'selected' => $this->_tpl_vars['filter']['continent']), $this);?>

	    </select>
	</div>

	<div class="boxfield">
	    <label>Country</label>
	    <select name="country">
	    	<option value="">All</value>
	    	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lists']['country'],'selected' => $this->_tpl_vars['filter']['country']), $this);?>

	    </select>
	</div>

	<div class="boxfield">
	    <label>Marital Status</label>
	    <select name="maritalstatus">
	    	<option value="">Both</option>
	    	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lists']['marital'],'selected' => $this->_tpl_vars['filter']['marital']), $this);?>

	    </select>
	</div>

	<div class="break"></div>

	<input type="submit" value="Search" name="button" />

	<!--<div>
		<input type="checkbox" name="fields[]" value="name" /> Name<br />
		<input type="checkbox" name="fields[]" value="surname" /> Surname<br />
		<input type="checkbox" name="fields[]" value="gender" /> Gender<br />
		<input type="checkbox" name="fields[]" value="age" /> Age<br />
		<input type="checkbox" name="fields[]" value="maritalStatus" /> Marital Status<br />
		<input type="checkbox" name="fields[]" value="weddingDate" /> Wedding Date<br />
		<input type="checkbox" name="fields[]" value="email" /> E-Mail<br />
		<input type="checkbox" name="fields[]" value="country" /> Country<br />
		<input type="checkbox" name="fields[]" value="city" /> City<br />
		<input type="checkbox" name="fields[]" value="POBox" /> POBox<br />
		<input type="checkbox" name="fields[]" value="citizenship" /> Citizenship<br />
		<input type="checkbox" name="fields[]" value="language" /> Languages<br />
		<input type="checkbox" name="fields[]" value="mobile" /> Mobile<br />
		<input type="checkbox" name="fields[]" value="occupation" /> Occupation<br />
		<input type="checkbox" name="fields[]" value="insU" /> Registration Date<br />
	</div>-->
</form>
</div>
<table class="list">
<thead>
<tr>
	<td>Name</td>
	<td>Sex</td>
	<td>Age</td>
	<td>Marital Status</td>
	<td>Country</td>
	<td>Citizenship</td>
	<td>Occupation</td>
	<td>Reg. Date</td>
</tr>
</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['people']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['name']; ?>
 <?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['surname']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['genderText']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['age']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['maritalStatusText']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['countryText']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['citizenship']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['occupation']; ?>
</td>
	<td><?php echo $this->_tpl_vars['people'][$this->_sections['list']['index']]['insTSDMY']; ?>
</td>
</tr>
<?php endfor; endif; ?>
</tbody>
<!--<tfoot>
<tr>
	<td>Username</td>
	<td>Name</td>
	<td>Profile</td>
</tr>
</tfoot>-->
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>