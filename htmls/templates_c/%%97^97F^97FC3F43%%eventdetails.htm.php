<?php /* Smarty version 2.6.10, created on 2014-06-04 10:43:49
         compiled from eventdetails.htm */ ?>
<div class="col-holder center">
<div class="col-1">	
		<div class="col-1-bot">
			<div class="sub-col-1">
				<div class="top">
					<div class="bot">
						<div class="overlay"></div>
						<table border="0" cellspacing="0" cellpadding="0">
							  <tbody>
								<tr>
									<td valign="middle" align="center" class="big-photo">
									<?php if (isset ( $this->_tpl_vars['match'][$this->_sections['list']['index']]['image'] )): ?>
							<img  width="62" height="62" alt="<?php echo $this->_tpl_vars['match'][0]['t1name']; ?>
 vs <?php echo $this->_tpl_vars['match'][0]['t2name']; ?>
" src="images/$match[0].image" >
							<?php else: ?>
							<img  class="big-photo" alt="<?php echo $this->_tpl_vars['match'][0]['t1name']; ?>
 vs <?php echo $this->_tpl_vars['match'][0]['t2name']; ?>
" src="images/moving ticket.gif" >
							<?php endif;  if ($this->_tpl_vars['match'][$this->_sections['list']['index']]['seats_sold'] >= $this->_tpl_vars['match'][$this->_sections['list']['index']]['allocatedtickets']): ?>
							  <div  id="bar"> <p class ="Sold">Sold Out </p></div>
							  <?php endif; ?>
									 
									</td>
								</tr>
								</tbody>
						</table>					
					</div>
				</div>
			</div>
			<h2 class="small">event information</h2>
			<div class="sub-col-1 sub-col-info">
				<div class="top">
					<div style="" class="bot event_info">
							<div id="wrap-event-information">
								<p><strong>Event Type: </strong>Sport, Soccer</p>
								<p><?php echo $this->_tpl_vars['match'][0]['t1name']; ?>
 vs <?php echo $this->_tpl_vars['match'][0]['t2name']; ?>
</p>
								<p><?php echo $this->_tpl_vars['match'][0]['description']; ?>
</p>
							</div>
							<br class="clear">
					</div>			
				</div>
			</div>
		</div>
</div>
		
<div class="col-2">
		<div id="event-summary" <?php if ($this->_tpl_vars['seats_sold'][0]['seats_sold'] >= $this->_tpl_vars['total_seats'][0]['total_seats']): ?>style="display:none;" style=" visibility: hidden;"<?php endif; ?>>
			
							<table border="0" cellspacing="0" cellpadding="0" class="booking_intermittent">
								<tbody>
										</tr><tr>
	<td colspan="2">
				<label>Select Section</label>
			</td>
</tr>
<tr>
	<td colspan="2">
					<select id="section" name="section"  onchange="price(this.value)">
					<option value="">select a section</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['match']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
	<option value="<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['name']; ?>
</option>
	
<?php endfor; endif; ?>		
							</select>
																				</td>
</tr>
	
<tr><td> Price : </td><Td >$<input type="text" id="price" readonly></td></tr>	
			<tr><td> No of Tickets : </td><Td><input type="text" name="numtick" id="numtick" onblur="totalprice(this.value,<?php echo $this->_tpl_vars['match'][0]['match_id']; ?>
,<?php echo $this->_tpl_vars['match'][0]['id']; ?>
)" ></td></tr>
			<tr><td>Total Amount :</td><Td>$<input type="text" id="totalamount" readonly> </td></tr>
			<tr><td colspan="2" id="image"  style="display:none;" style=" visibility: hidden;" >
			<div id="greylink"></div>
			
			</td>
			
			</tr>
							</tbody>
							</table>
						
							<br class="clear">
					
		</div>
		
		<div class="tab-wrap">
			<div id="tab_detail" class="tab">
				<div class="tab-wrap-in">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="additional-info"><tbody><tr><td>
					<div id="tab_detail_div">
						
<div class="expandable">
<h2>Venues and performances</h2>
	<h4><?php echo $this->_tpl_vars['match'][0]['vname']; ?>
</h4>
	<p id="occurrence">
			<a href="/web/event/the_fray/812317323/412076216/"><?php echo $this->_tpl_vars['match'][0]['match_date']; ?>
</a><br>
		</p>
</div>

<div class="extra-info">			BOOKING OPENS 09:00 ON FRIDAY 28 MARCH. <b><b><br>
<br>
The Fray is Isaac Slade, Joe King (guitar and vocals), Dave Welsh (guitar) and Ben Wysocki (drums). The Denver based foursome formed in 2002 after high school friends Slade and King bumped into each other at the local guitar shop. The band achieved national success with their first single `Over My Head`, which became a top ten hit in the U.S but the release of their second single `How To Save A Life` brought them into the worldwide spotlight going double platinum and earning a 2010 Grammy nomination for their self titled release. The recently released fourth album `Helios`, was produced by Stuart Price (The Killers, Madonna, Keane) and Ryan Tedder  (Adele and One Republic). The latest single `Love Don`t Die`, received critical acclaim from Billboard Magazine and is currently on the KIA Top 40 on 94.7 Highveld Stereo and 94.5 KFM.	</b></b></div>
				</div>
					</td></tr></tbody></table>
				</div>
			</div>
		</div>
		
	</div>
	</div>