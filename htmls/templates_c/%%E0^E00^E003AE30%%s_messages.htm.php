<?php /* Smarty version 2.6.10, created on 2011-01-14 13:25:50
         compiled from s_messages.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 's_messages.htm', 39, false),)), $this); ?>
<div class="tableFilter">
<form action="index.php?c=index" method="post">
	<div class="boxfield">
	    <label>Date</label>
	    <input type="text" name="insTS" class="_fDate" value="<?php echo $this->_tpl_vars['filter']['insTS']; ?>
" />
	</div>

	<div class="boxfield">
	    <label>Title/Text</label>
	    <input type="text" name="message" class="" value="<?php echo $this->_tpl_vars['filter']['message']; ?>
" />
	</div>


	<div class="break"></div>

	<input type="submit" value="Search" name="button" />

</div>

<table class="list">
<thead>
<tr>
	<td>Name</td>
	<td>Surname</td>
	<td>Country</td>
	<td>Date</td>
	<td>Message</td>
	<td>Actions</td>

</tr>
</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['messages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>
	<td><?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['name']; ?>
</td>
	<td><?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['surname']; ?>
</td>	
	<td><?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['commonName']; ?>
</td>
	<td><?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['insTS']; ?>
</td>
	<td><?php echo ((is_array($_tmp=$this->_tpl_vars['messages'][$this->_sections['list']['index']]['message'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30) : smarty_modifier_truncate($_tmp, 30)); ?>
</td>
	<td>
		<a href="index.php?c=message-edit&id=<?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/edit.png" /></a>
		<!--<a href="javascript:messageDelete(<?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['id']; ?>
)" /><img src="images/icons/delete.png" /></a>-->
		<a href="do.messages.php?op=S&id=<?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['id']; ?>
" /><img src="images/icons/status<?php echo $this->_tpl_vars['messages'][$this->_sections['list']['index']]['active']; ?>
.png" /></a>
	</td>
</tr>
<?php endfor; endif; ?>
</tbody>
</table>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>