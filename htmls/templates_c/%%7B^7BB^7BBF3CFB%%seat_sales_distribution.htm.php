<?php /* Smarty version 2.6.10, created on 2014-04-13 11:15:43
         compiled from seat_sales_distribution.htm */ ?>
<div id="main">
<div id="content">
<div id="top_con"><div id="page_title"> Match Management</div>
<a href="#">Table</a>
</div>
<table class="table">
	<thead class="header_table">
				<th  width="2%">Match ID</th>
				<th  width="10%">Venue</th>
				<th  width="54%">Match</th>
				<th  width="8%">Date</th>
				<th  width="8%">Section</th>
				<th  width="18%">Ticket Sales</th>
				<th  width="18%">Ticket Price</th>
				<th  width="18%">Total Cost</th>
			
	</thead>
<tbody>
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['match']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>

	
	 <td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['id']; ?>
</td>
	 <td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['vname']; ?>
</td>
	<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['t1name']; ?>
 vs <?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['t2name']; ?>
</td>
	<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['match_date']; ?>
</td>
<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['section']; ?>
</td>
	<td><?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['counter']; ?>
</td>
	<td>$<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['amount']; ?>
</td>
	<td>$<?php echo $this->_tpl_vars['match'][$this->_sections['list']['index']]['total_charge']; ?>
</td>
</tr>
<?php endfor; endif; ?>
</tbody>
<tfoot>
<tr>

	 <td></td>
	 <td></td>
	<td></td>
	<td></td>
<td></td>
	<td></td>
	<th>Total</th>
	<th>$<?php echo $this->_tpl_vars['totalcost'][0]['sumcost']; ?>
</th>
</tr>
</tfoot>
</table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div></div>