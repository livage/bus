<?php /* Smarty version 2.6.10, created on 2015-10-21 12:21:14
         compiled from route_edit.htm */ ?>

<form action="do.route.php?id=<?php echo $this->_tpl_vars['route'][0]['id']; ?>
" method="post" enctype="multipart/form-data">
	<div id="content">
	
        <fieldset >
		
			<legend>Route</legend>
			
	        <input type="hidden" name="op"  value="<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
" />
			
			<div class="boxfield">
			 	<label for="code">Code:</label> <input type="text" name="code" size="70" value="<?php echo $this->_tpl_vars['route'][0]['code']; ?>
" required/>
				
			 </div>
			
			<div class="boxfield">
<label for="stationA">Departure Station:</label> 
				
				<select  NAME="stationA" width="700" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['station']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<?php if ($this->_tpl_vars['station'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['route'][0]['stationA']): ?>
<option VALUE="<?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['station_name']; ?>
</option>
<?php else: ?>
					<option VALUE="<?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['station_name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>

			</div>
			
				
			<div class="boxfield">
				<label for="stationB">Arrival Station:</label> 
				
				<select  NAME="stationB" width="700" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['station']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<?php if ($this->_tpl_vars['station'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['route'][0]['stationB']): ?>
<option VALUE="<?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['station_name']; ?>
</option>
<?php else: ?>
					<option VALUE="<?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['station'][$this->_sections['list']['index']]['station_name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>

			</div>
		
			
			<div class="boxfield">
				<label for="cost">Cost:</label>  <input type="text" name="cost" size="70" value="<?php echo $this->_tpl_vars['route'][0]['cost']; ?>
" required/>
			</div>
			
			<div class="boxfield">
				<label for="duration">Duration:</label>  <input type="text" name="duration" size="70" value="<?php echo $this->_tpl_vars['route'][0]['duration']; ?>
" required/>  (in Minutes)
			</div>
			
			
			<div class="boxfield">
			 	
				<input type="checkbox" name ="has_subroutes" id="chkbox" <?php if ($this->_tpl_vars['route'][0]['has_subroutes'] == 1): ?> checked<?php endif; ?> >Sub routes
			 </div>
			  
			 <?php if ($this->_tpl_vars['route'][0]['has_subroutes'] == 0 || $this->_tpl_vars['op'] == I): ?> 
			<div id="image"  style="display: none;" style=" visibility: hidden;" class="boxfield">
			<?php endif; ?>
				<!--<div class="boxfield">
				Sub Route:
				
				<select  NAME="subroute" width="700" >
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['sroute']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
						<?php if ($this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['sroute'][0]['code']): ?>
							<option VALUE="<?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php else: ?>
						<option VALUE="<?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php endif; ?>
					<?php endfor; endif; ?>
				</select>

			</div>-->
			<div class="boxfield">
				 <table class="table">
	<thead class="pricing">
				<th  width="50%">Route</th>
			
			<!--	<th  width="10%">Price</th>
				
			<th  width="10%">No of Seats</th>-->
	</thead>
<tbody id="pricing">
<?php if ($this->_tpl_vars['op'] == I): ?>
<tr>

	<td>
				
				<select  NAME="route[]" width="700" >
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['sroute']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
						<?php if ($this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['route'][0]['code']): ?>
							<option VALUE="<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php else: ?>
						<option VALUE="<?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php endif; ?>
					<?php endfor; endif; ?>
				</select>
	</td>
	
	<td><input type="text" name="price[]" value="<?php echo $this->_tpl_vars['seats'][$this->_sections['list']['index']]['price']; ?>
" required /></td>
	<!--<td><input type="text" name="allocation[]" value="<?php echo $this->_tpl_vars['seats'][$this->_sections['list']['index']]['allocation']; ?>
" required /></td>-->
	
	
</tr>
<?php endif; ?>

<?php unset($this->_sections['lista']);
$this->_sections['lista']['name'] = 'lista';
$this->_sections['lista']['loop'] = is_array($_loop=$this->_tpl_vars['subroute']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['lista']['show'] = true;
$this->_sections['lista']['max'] = $this->_sections['lista']['loop'];
$this->_sections['lista']['step'] = 1;
$this->_sections['lista']['start'] = $this->_sections['lista']['step'] > 0 ? 0 : $this->_sections['lista']['loop']-1;
if ($this->_sections['lista']['show']) {
    $this->_sections['lista']['total'] = $this->_sections['lista']['loop'];
    if ($this->_sections['lista']['total'] == 0)
        $this->_sections['lista']['show'] = false;
} else
    $this->_sections['lista']['total'] = 0;
if ($this->_sections['lista']['show']):

            for ($this->_sections['lista']['index'] = $this->_sections['lista']['start'], $this->_sections['lista']['iteration'] = 1;
                 $this->_sections['lista']['iteration'] <= $this->_sections['lista']['total'];
                 $this->_sections['lista']['index'] += $this->_sections['lista']['step'], $this->_sections['lista']['iteration']++):
$this->_sections['lista']['rownum'] = $this->_sections['lista']['iteration'];
$this->_sections['lista']['index_prev'] = $this->_sections['lista']['index'] - $this->_sections['lista']['step'];
$this->_sections['lista']['index_next'] = $this->_sections['lista']['index'] + $this->_sections['lista']['step'];
$this->_sections['lista']['first']      = ($this->_sections['lista']['iteration'] == 1);
$this->_sections['lista']['last']       = ($this->_sections['lista']['iteration'] == $this->_sections['lista']['total']);
?>
<tr>

	<td>
				
				<select  NAME="route[]" width="700" >
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['sroute']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
						<?php if ($this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['subroute'][$this->_sections['lista']['index']]['subroute']): ?>
							<option VALUE="<?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php else: ?>
						<option VALUE="<?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['sroute'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php endif; ?>
					<?php endfor; endif; ?>
				</select>
	</td>
	
	<!--<td><input type="text" name="price[]" value="<?php echo $this->_tpl_vars['subroute'][$this->_sections['list']['index']]['price']; ?>
" required /></td>
	<td><input type="text" name="allocation[]" value="<?php echo $this->_tpl_vars['seats'][$this->_sections['list']['index']]['allocation']; ?>
" required /></td>-->
	
	
</tr>
<?php endfor; endif; ?>

</tbody>

</table>
		
				
			 <input type="button" value="Add" onClick="return newupload()"> 
			 </div>
			 <?php if ($this->_tpl_vars['route'][0]['has_subroutes'] == 0 || $this->_tpl_vars['op'] == I): ?>
			</div>
			<?php endif; ?>
        </fieldset>
	
	   
		<input type="submit" value="Confirm" class="serviceSub row_btn"/>
	    <input type="button" value="Cancel" class="serviceCan row_bt" href="route_list.php" />
	</div>
</form>

