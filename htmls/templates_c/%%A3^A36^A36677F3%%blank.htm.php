<?php /* Smarty version 2.6.10, created on 2015-12-14 11:21:19
         compiled from template/blank.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'json_encode', 'template/blank.htm', 55, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Bus Stop</title>
<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon'/ >
<link rel="stylesheet" type="text/css" href="css/core.css"></link>
<link rel="stylesheet" type="text/css" href="css/layout.css"></link>
<link rel="stylesheet" type="text/css" href="css/all.css?nocache=28"></link>
<link rel="stylesheet" type="text/css" href="css/elements.css?nocache=4"></link>
<link rel="stylesheet" type="text/css" href="css/print.css?nocache=4"></link>

<!--************************************** grey box  *****************************************-->
		<script type="text/javascript">
    var GB_ROOT_DIR = "greybox/";
</script>

<script type="text/javascript" src="greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/gb_scripts.js"></script>
<link href="greybox/gb_styles.css" rel="stylesheet" type="text/css" />

<link href="css/modern.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="js/comp/swfobject.js"></script>

<script type="text/javascript" src="js/comp/jsctkmi.js?nocache=8"></script>-->



<link rel="stylesheet" type="text/css" href="css/general.css"></link>
<script type="text/javascript" src="libs/jquery/jquery.js"></script>
<script type="text/javascript" src="js/_tabs.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>
<script type="text/javascript" src="libs/jquery/jquery.maskedinput.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/initPage.js"></script>

<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="css/jquery.ui.datepicker.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.base.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.core.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.theme.css"></link>

<script type="text/javascript" src="libs/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datePicker.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>
<link href="libs/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
<?php if ($this->_tpl_vars['_ENGINE']['contentClass']): ?>
<link rel="stylesheet" type="text/css" href="css/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.css"></link>
<script type="text/javascript" src="js/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.js"></script>
<?php endif; ?>
<?php if ($this->_tpl_vars['_ENGINE']['contentClass'] == bus): ?>
<script type="text/javascript">
 bookedSeats = <?php echo json_encode($this->_tpl_vars['booked_seats']); ?>
;
</script>
<?php endif; ?>
</head>
<body>
<!--<div id="header"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/o_header.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div> -->
<div class="clear"></div>
<div id="container">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['page_content'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>

<div class="clear"></div>
<div id="footer">
        <div class="nav-bar bg-color-darken">
            <div class="nav-bar-inner padding10">
                           <div class="place-right">
                           		<img width="50" height="50"  src="images/proudly zimbabwean.jpg" class="place-left">
                                <span class="element">Proudly Zimbabwean</span>
                           </div>
                <span class="element center">
                	Copyright &copy; Caveman Solutions 2014
               </span>

            </div>

        </div>
    </div>

</html>
</body>