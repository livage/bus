<?php /* Smarty version 2.6.10, created on 2015-12-04 16:05:14
         compiled from template/front_desk.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'json_encode', 'template/front_desk.htm', 105, false),)), $this); ?>
<?php echo '<?xml'; ?>
 version="1.0" encoding="UTF-8"<?php echo '?>'; ?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"><head id="j_idt2">
	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Bus Stop</title>
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,600" rel="stylesheet" type="text/css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet" type="text/css">

<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' ></link>

<link href="css/public.css" rel="stylesheet" type="text/css" ></link>
<link rel="stylesheet" type="text/css" href="css/layout.css"></link>
	<!--<link rel="stylesheet" type="text/css" href="css/menu.css"></link>-->
<link rel="stylesheet" type="text/css" href="css/buttons-divs-pag.css"></link>
<link rel="stylesheet" type="text/css" href="css/form.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="css/core.css"></link>

<script   src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" /> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script  src="js/jquery.ssd-vertical-navigation.min.js"></script>
<script src="js/app.js"></script>
<!--<script type="text/javascript" src="libs/jquery/jquery.js"></script>-->


<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/initPage.js"></script>
<!--<script type="text/javascript" src="js/common.js"></script>-->
<script type="text/javascript" src="js/hide.js" ></script>

<!--<script type="text/javascript" src="libs/jquery/jquery.js"></script>-->
<script type="text/javascript" src="libs/jquery/jquery.maskedinput.js"></script>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="css/jquery.ui.datepicker.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.base.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.core.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.theme.css"></link>
	


<script type="text/javascript" src="libs/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datePicker.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>
<link href="libs/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">
    
	
	


<!--

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>    
 <script  src="js/jquery.ssd-vertical-navigation.min.js"></script>
<script src="js/app.js"></script>   
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" /> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>




<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/initPage.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/hide.js" ></script>

<script type="text/javascript" src="libs/jquery/jquery.js"></script>
<script type="text/javascript" src="libs/jquery/jquery.maskedinput.js"></script>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="css/jquery.ui.datepicker.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.base.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.core.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.theme.css"></link>
	


<script type="text/javascript" src="libs/jquery/jquery.autocomplete.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datePicker.js"></script>
<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>
<link href="libs/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">


-->

<?php if ($this->_tpl_vars['_ENGINE']['contentClass']): ?>
<link rel="stylesheet" type="text/css" href="css/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.css"></link>
<script type="text/javascript" src="js/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.js"></script>
<?php endif;  if ($this->_tpl_vars['_ENGINE']['contentClass'] == bus): ?>
<script type="text/javascript">
 bookedSeats = <?php echo json_encode($this->_tpl_vars['booked_seats']); ?>
;
</script>
<?php endif; ?>
<!--<link rel="stylesheet" type="text/css" href="datetimepicker/jquery.datetimepicker.css"/>
<script src="datetimepicker/jquery.js"></script>
<script src="datetimepicker/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="js/picker.js" ></script>
	
-->


	<style type="text/css">
	<?php echo '
#line-chart {
	height: 300px;
	width: 800px;
	margin: 0px auto;
	margin-top: 1em;
}

.brand {
	font-family: georgia, serif;
}

.brand .first {
	color: #ccc;
	font-style: italic;
}

.brand .second {
	color: #fff;
	font-weight: bold;
}
'; ?>

</style>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]&gt;
      &lt;script src="http://html5shim.googlecode.com/svn/trunk/html5.js"&gt;&lt;/script&gt;
    &lt;![endif]-->

	<!-- Le fav and touch icons -->
 	<link rel="shortcut icon" href="resources/img/favicon.png" />
	
	<script type="text/javascript">
	<?php echo '
    window.history.forward();
    function noBack() { window.history.forward(); }
	'; ?>

</script>
<link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="css/theme.css" />

</head>
<body onload="noBack();" onunload="">

	<div class="navbar">
		<div class="navbar-inner">
			<ul class="nav pull-right">
				<li id="fat-menu" class="dropdown"><a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i> <?php echo $this->_tpl_vars['userdata']['username']; ?>
 <i class="fa fa-caret-down"></i>
				</a>

					<ul class="dropdown-menu">
						<li><a tabindex="-1" href="profile.xhtml"><i class="fa fa-user"></i> Profile</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" href="core/do.logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
					</ul></li>

			</ul>
			<a class="brand" href="index.html"><span class="first">Bus</span> <span class="second">Stop </span></a>
		</div>
	</div>


	<div class="sidebar-nav">
		<form class="search form-inline">
			<input type="text" placeholder="Search..." />
		</form>
				<a href="#dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-dashboard"></i> <?php echo $this->_tpl_vars['_ENGINE']['contentTitle']; ?>
</a>
			
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/frontdesk_menu.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>



	<div class="content">

		<div class="header">
			

			<h1 class="page-title"><i class="fa fa-dashboard"></i> Dashboad
			</h1>
		</div>

		<ul class="breadcrumb">
		<li class="active"><a href="dashboard.xhtml">Dashboard</a></li>
		</ul>

		<div class="container-fluid">
			<div class="row-fluid">
	
	
	<div class="row-fluid">

  

    <div class="block">
        <a href="#page-stats" class="block-heading" data-toggle="collapse"><?php echo $this->_tpl_vars['_ENGINE']['contentTitle']; ?>
</a> 
        <div id="page-stats" class="block-body collapse in">
        
        
			
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['page_content'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

        </div>
    </div>
</div>
				<div id="chat_div"></div>
				
				<div class="clear"></div>
<div id="footer">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/a_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
				

			</div>
		</div>
	</div>
	

</body>

</html>