<?php /* Smarty version 2.6.10, created on 2015-12-14 11:47:37
         compiled from box/pagination.htm */ ?>

	<div class="text">Results: <strong><?php echo $this->_tpl_vars['results']; ?>
</strong> </div>
	<?php if ($this->_tpl_vars['pages'] > 1): ?>
		
		
		<ul id="pagination-flickr">
		<?php if ($this->_tpl_vars['page'] == 1): ?>
<li class="previous-off"><< Previous</li>
<?php else: ?>
<li class="previous"><a href="index.php?c=<?php echo $this->_tpl_vars['_ENGINE']['content']; ?>
&pg=<?php echo $this->_tpl_vars['page']-1; ?>
"> << Previous </a></li>
<?php endif; ?>

<?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['pageRange']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['step'] = 1;
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = $this->_sections['index']['loop'];
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
			<?php if ($this->_tpl_vars['pageRange'][$this->_sections['index']['index']] == $this->_tpl_vars['page']): ?>
			<li class="active"><?php echo $this->_tpl_vars['pageRange'][$this->_sections['index']['index']]; ?>
</li>
			<?php else: ?>
			<li><a href="index.php?c=<?php echo $this->_tpl_vars['_ENGINE']['content']; ?>
&pg=<?php echo $this->_tpl_vars['pageRange'][$this->_sections['index']['index']]; ?>
"><?php echo $this->_tpl_vars['pageRange'][$this->_sections['index']['index']]; ?>
</a></li>
			<?php endif; ?>
	    <?php endfor; endif; ?>
		
		<?php if ($this->_tpl_vars['page'] == $this->_tpl_vars['pages']): ?>
		<li class="next-off"> Next >></li>
		<?php else: ?>
	<li class="next"><a href="index.php?c=<?php echo $this->_tpl_vars['_ENGINE']['content']; ?>
&pg=<?php echo $this->_tpl_vars['page']+1; ?>
"> Next >></a></li>
	<?php endif; ?>
</ul> 	
	<?php endif; ?>

		
		
		
		
		
	