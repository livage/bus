<?php /* Smarty version 2.6.10, created on 2014-05-14 10:28:04
         compiled from match_edit.htm */ ?>

<form action="do.match.php?id=<?php echo $this->_tpl_vars['match'][0]['id']; ?>
" method="post" enctype="multipart/form-data">
	<div id="content">
	
        <fieldset >
		
			<legend>Match</legend>
			
	        <input type="hidden" name="op"  value="<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
" />
			
			<div class="boxfield">
			 	Date: <input id="datetimepicker2" type="text" name="match_date"  value="<?php echo $this->_tpl_vars['match'][0]['mdate']; ?>
" required/>
				
			 </div>
			 
			 <div  class="boxfield">
				Time: <input id="datetimepicker1" type="text" name="match_time"  value="<?php echo $this->_tpl_vars['match'][0]['match_time']; ?>
 " required/>
			</div>
			
			<div class="boxfield">
				Venue:
				
				<select  NAME="venue" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['venue']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<?php if ($this->_tpl_vars['venue'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['match'][0]['venue']): ?>
<option VALUE="<?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['name']; ?>
</option>
<?php else: ?>
					<option VALUE="<?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['venue'][$this->_sections['list']['index']]['name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>

			</div>
		
			<div  class="boxfield">
				Team 1: <select  NAME="team1" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['team']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>

					<?php if ($this->_tpl_vars['team'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['match'][0]['team1']): ?>
<option VALUE="<?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['name']; ?>
</option>
<?php else: ?>

					<option VALUE="<?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>
			</div>
			  
			<div class="boxfield">
				Team 2:  <select  NAME="team2" required>
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['team']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<?php if ($this->_tpl_vars['team'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['match'][0]['team2']): ?>
<option VALUE="<?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['name']; ?>
</option>
<?php else: ?>

					<option VALUE="<?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['team'][$this->_sections['list']['index']]['name']; ?>
</option>
					<?php endif; ?>
					<?php endfor; endif; ?>
				</select>
			</div>
			  
			<div class="boxfield">
				Referee: <input type="text" name="referee" size="70" value="<?php echo $this->_tpl_vars['match'][0]['referee']; ?>
" required/>
			</div>
			
			<div class="boxfield">
				Description: <textarea name="description"  required><?php echo $this->_tpl_vars['match'][0]['description']; ?>
</textarea>
			</div>
		
        </fieldset>
		 <fieldset>
		 <legend>Seat Pricing</legend>
		 <table class="table">
	<thead class="pricing">
				<th  width="50%">Name</th>
			
				<th  width="10%">Price</th>
				
			<th  width="10%">No of Seats</th>
	</thead>
<tbody id="pricing">
<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['seats']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
<tr>

	<td><input type="text" name="name[]"  value="<?php echo $this->_tpl_vars['seats'][$this->_sections['list']['index']]['name']; ?>
" required /></td>
	
	<td><input type="text" name="price[]" value="<?php echo $this->_tpl_vars['seats'][$this->_sections['list']['index']]['price']; ?>
" required /></td>
	<td><input type="text" name="allocation[]" value="<?php echo $this->_tpl_vars['seats'][$this->_sections['list']['index']]['allocation']; ?>
" required /></td>
	
	
</tr>
<?php endfor; endif; ?>
</tbody>

</table>
		  
			 <input type="button" value="Add" onClick="return newupload()"> 
			 </div>
		  </fieldset>
	   
		<input type="submit" value="Confirm" class="serviceSub row_btn"/>
	    <input type="button" value="Cancel" class="serviceCan row_bt" href="match_list.php" />
	</div>
</form>
