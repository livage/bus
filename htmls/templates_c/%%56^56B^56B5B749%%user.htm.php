<?php /* Smarty version 2.6.10, created on 2011-01-20 15:41:38
         compiled from template/user.htm */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>KindomWindow-User</title>
<link rel="stylesheet" type="text/css" href="css/core.css"></link>
<link rel="stylesheet" type="text/css" href="css/user.css"></link>
<link rel="stylesheet" type="text/css" href="css/common.css"></link>


<script type="text/javascript" src="libs/jquery/jquery.js"></script> 
<script type="text/javascript" src="libs/jquery/jquery.autocomplete.js "></script>
<script type="text/javascript" src="libs/jquery/ui.datepicker.js"></script>
<script type="text/javascript" src="libs/jquery/jquery.maskedinput.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/initPage.js"></script>

<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.autocomplete.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="css/jquery.ui.datepicker.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.all.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.base.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.core.css"></link>
<link rel="stylesheet" type="text/css" href="libs/jquery/jquery.ui.theme.css"></link


<?php if ($this->_tpl_vars['_ENGINE']['contentClass']): ?>
<link rel="stylesheet" type="text/css" href="css/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.css"></link>
<script type="text/javascript" src="js/<?php echo $this->_tpl_vars['_ENGINE']['contentClass']; ?>
.js"></script>
<?php endif; ?>
</head>
<body>
<div id="header">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_header.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<div id="container">
	<div id="left">
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_profile.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    	<div class="numberofblessings"><img src="images/cross.jpg" /><div>15 Number of blessings for the day<div class="date">08/12/2010</div></div></div>
    	<div class="linksCon">
			<a href="index.php?c=search_friends" id="inviteafriend"><div class="first">search for friends</div></a>
    		<a href="#" id="inviteafriend"><img src="images/dove_black.png" class="blackdove" /><div class="first">Invite a friend</div></a>
			
    		<a href="index.php?c=index" id="messages"><img src="images/email.png" class="messageimg" /><div class="second">Messages</div></a>
    	</div>
    	<?php if (! $this->_tpl_vars['userdata']['person']['acceptJesus'] && ! $this->_tpl_vars['userdata']['person']['alreadyBorn']): ?>
    	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_salvation.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    	<?php endif; ?>
    	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_notifications.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    	<!-- end notificationsCon -->

    	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_WBBNews.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<!-- end wbbnews -->

		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_wedding.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
    <div id="main">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['page_content'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</div>
	<div class="clear"></div>
</div>
<div id="footer">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box/u_footer.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
</body>
</html>