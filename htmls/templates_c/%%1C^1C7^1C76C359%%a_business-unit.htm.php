<?php /* Smarty version 2.6.10, created on 2011-01-21 11:12:41
         compiled from a_business-unit.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'a_business-unit.htm', 8, false),)), $this); ?>
<div class="searchCon">
    <a href="index.php?c=business-unit-new" class="button"><img src="" /><div>Nuovo</div></a>
</div>
<div id="main">
    <div id="list">
        <select  name = "listame" onchange="window.location ='index.php?c=business-unit-list&list='+$(this).val();">
            <option>-- Seleziona un elenco --</option>
            <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['listValues'],'selected' => $this->_tpl_vars['listnam']), $this);?>

        </select>
    </div>
    
    <table class="table_results">
        <thead class="header_table">
            <tr>
                <td style="width:300px">Value</td>
                <td style="width:300px">Label</td>
                <td style="width:200px"></td>
            </tr>
        </thead>
    </table>
    <?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['good']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
     <div class="tableRow <?php if ($this->_sections['list']['index'] % 2 == 0): ?> even <?php else: ?> odd <?php endif; ?> <?php if ($this->_tpl_vars['good'][$this->_sections['list']['index']]['active'] == 0): ?> disabled <?php endif; ?>">
        <table>
            <tbody>
                <tr>
                    <td style="width:300px"><?php echo $this->_tpl_vars['good'][$this->_sections['list']['index']]['list_value']; ?>
</td>
                    <td style="width:300px"><?php echo $this->_tpl_vars['good'][$this->_sections['list']['index']]['list_label']; ?>
</td>
                    <td style="width:200px"><a href="do.business.php?op=S&id=<?php echo $this->_tpl_vars['good'][$this->_sections['list']['index']]['id']; ?>
" class="enable"><img src="images/bulb_<?php echo $this->_tpl_vars['good'][$this->_sections['list']['index']]['active']; ?>
.png" /></a></td>
                </tr>
            </tbody>
        </table>
       
    </div>
    <?php endfor; endif; ?>
    <div class="pagination">
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'pagination.htm', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>        
    </div>
</div>