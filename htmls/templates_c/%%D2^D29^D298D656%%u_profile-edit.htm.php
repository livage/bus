<?php /* Smarty version 2.6.10, created on 2011-01-14 15:17:30
         compiled from u_profile-edit.htm */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'u_profile-edit.htm', 17, false),)), $this); ?>
<p class="pageTitle">Edit Profile (<?php echo $this->_tpl_vars['opString']; ?>
)</p>

<form action="do.profile.php?op=<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
&id=<?php echo $this->_tpl_vars['profile']['id']; ?>
" method="post">
	
	<div class="break"></div>

	<div class="boxfield">
	    <label>Name</label>
	   <input type="text" name="name" id="name" value="<?php echo $this->_tpl_vars['profile']['name']; ?>
 "/>
	<!-- <?php echo $this->_tpl_vars['userdata']['person']['name']; ?>
 <?php echo $this->_tpl_vars['userdata']['person']['surname']; ?>
-->
	</div>
	<div class="break"></div>
	<div class="boxfield">
	    <label>Gender:</label>
	    <select name="gender">
	    	<option value="">All</value>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['_lists']['gender'],'separator' => ' ','selected' => $this->_tpl_vars['profile']['gender']), $this);?>

	    </select>
	</div>
	
	<div class="boxfield date">
		<label>Marital status</label>
		<select name="maritalStatus">
			<option value="">All</value>
			<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['_lists']['marital'],'selected' => $this->_tpl_vars['profile']['maritalStatus']), $this);?>

	</select>
	</div>
	<div class="break"></div>
	
	<div class="boxfield">
	    <label>Date of Birth</label>
	      <input class="_fDate" type="text" name="dateOfBirth" value="<?php echo $this->_tpl_vars['profile']['dateOfBirth']; ?>
" id="dateOfBirthDMY"/>
	</div>
<div class="break"></div>
	<div class="boxfield">
	    <label>PO Box</label>
	   <input type="text" name="POBox" id="POBox" value="<?php echo $this->_tpl_vars['profile']['POBox']; ?>
" />
	</div>
	<div class="break"></div>
	<div class="break"></div>
		<div class="boxfield">
	    <label>Email</label>
	   <input type="text" name="email" id="email" value="<?php echo $this->_tpl_vars['profile']['email']; ?>
" />
	 
	</div>
	<div class="break"></div>
		<div class="boxfield">
	    <label>Mobile Phone Number:</label>
	   <input type="text" name="mobile" id="mobile" value="<?php echo $this->_tpl_vars['profile']['mobile']; ?>
" />
	 
	</div>
	<div class="break"></div>
	<div class="boxfield">
	    <label>Languages</label>
	   <input type="text" name="languages" id="mobile" value="<?php echo $this->_tpl_vars['profile']['languages']; ?>
" />
	 
	</div>
	<div class="break"></div>
	<div class="boxfield">
	    <label>Occupation</label>
	    <input type="text" name="occupation" id="occupation" value="<?php echo $this->_tpl_vars['profile']['occupation']; ?>
" />
	</div>
	<div class="break"></div>
	<div class="boxfield">
		<label>Country:</label>
		<select name="country" class="_fCountry">
	        <option value="">All</value>
	        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['_lists']['countries'],'selected' => $this->_tpl_vars['profile']['country']), $this);?>

	   <!--     <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['lists']['country'],'selected' => $this->_tpl_vars['new']['country']), $this);?>
-->
	    </select>
	</div>
	
	<div class="break"></div>

	<input type="submit" value="Confirm" class="serviceSub row_btn"/>
	

</form>



