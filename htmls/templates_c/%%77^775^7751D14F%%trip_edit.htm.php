<?php /* Smarty version 2.6.10, created on 2015-10-21 12:28:17
         compiled from trip_edit.htm */ ?>
<form action="do.trip.php?id=<?php echo $this->_tpl_vars['trip'][0]['id']; ?>
" method="post" enctype="multipart/form-data">
	<div id="content">
	
        <fieldset >
		
			<legend>Trip</legend>
			
	        <input type="hidden" name="op"  value="<?php echo $this->_tpl_vars['_ENGINE']['operation']; ?>
" />
			
			<div class="fieldbox">
			<div class="boxfield">
			 	<label for="name"> Name:</label> <input type="text" name="name" size="70" value="<?php echo $this->_tpl_vars['trip'][0]['name']; ?>
" required/>
				</div>
				</div>
						
		
			<div  class="fieldbox ">
				<div class="boxfield">
				<label for="driver"> Driver:</label> <input type="text" name="driver" size="70" value="<?php echo $this->_tpl_vars['trip'][0]['driver']; ?>
" required/>
				</div>
			</div>
			  <div  class="fieldbox ">
			<div class="boxfield">
				<label for="bus_no">Bus No:</label> <input type="text" name="bus_no" size="70" value="<?php echo $this->_tpl_vars['trip'][0]['bus_no']; ?>
" required/>
			</div>
			  </div>
			  			  <div  class="fieldbox ">
			<div  class="boxfield">
				<label for="bus_seats">Bus seats:</label> <input type="text" name="bus_seats" size="70" value="<?php echo $this->_tpl_vars['trip'][0]['bus_seats']; ?>
 " required/>
			</div>
			</div>
						  <div  class="fieldbox ">
			<div  class="boxfield">
				<label for="departure_date">Departure Date:</label> <input type="text" name="departure_date" id="datetimepicker2" value="<?php echo $this->_tpl_vars['trip'][0]['departure_date']; ?>
 " required/>
			</div>
			</div>
						  <div  class="fieldbox ">
			<div  class="boxfield">
			<!--Time: <input id="datetimepicker1" type="text" name="departure_time"  value="<?php echo $this->_tpl_vars['match'][0]['match_time']; ?>
 " required/>-->
<label for="departure_time">Departure Time:</label> <input type="text" name="departure_time" id="datetimepicker1" value="<?php echo $this->_tpl_vars['trip'][0]['departure_time']; ?>
 " required/>
			</div>
		</div>
		<table>
		<tr>
<td></td>
	<td>
			Main Route :	
			<?php if ($this->_tpl_vars['op'] == I): ?> 
				<select  NAME="route" width="700"   onchange="showUser(this.value)" >
					<option VALUE="">----</option>
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['route']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
						<?php if ($this->_tpl_vars['route'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['trip'][0]['route']): ?>
							<option VALUE="<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
" selected ><?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php else: ?>
						<option VALUE="<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['id']; ?>
"><?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['code']; ?>
</option>
						<?php endif; ?>
					<?php endfor; endif; ?>
				</select>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['op'] == U): ?>
				
					<?php unset($this->_sections['list']);
$this->_sections['list']['name'] = 'list';
$this->_sections['list']['loop'] = is_array($_loop=$this->_tpl_vars['route']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['list']['show'] = true;
$this->_sections['list']['max'] = $this->_sections['list']['loop'];
$this->_sections['list']['step'] = 1;
$this->_sections['list']['start'] = $this->_sections['list']['step'] > 0 ? 0 : $this->_sections['list']['loop']-1;
if ($this->_sections['list']['show']) {
    $this->_sections['list']['total'] = $this->_sections['list']['loop'];
    if ($this->_sections['list']['total'] == 0)
        $this->_sections['list']['show'] = false;
} else
    $this->_sections['list']['total'] = 0;
if ($this->_sections['list']['show']):

            for ($this->_sections['list']['index'] = $this->_sections['list']['start'], $this->_sections['list']['iteration'] = 1;
                 $this->_sections['list']['iteration'] <= $this->_sections['list']['total'];
                 $this->_sections['list']['index'] += $this->_sections['list']['step'], $this->_sections['list']['iteration']++):
$this->_sections['list']['rownum'] = $this->_sections['list']['iteration'];
$this->_sections['list']['index_prev'] = $this->_sections['list']['index'] - $this->_sections['list']['step'];
$this->_sections['list']['index_next'] = $this->_sections['list']['index'] + $this->_sections['list']['step'];
$this->_sections['list']['first']      = ($this->_sections['list']['iteration'] == 1);
$this->_sections['list']['last']       = ($this->_sections['list']['iteration'] == $this->_sections['list']['total']);
?>
						<?php if ($this->_tpl_vars['route'][$this->_sections['list']['index']]['id'] == $this->_tpl_vars['trip'][0]['route']): ?>
							<input type="text" VALUE="<?php echo $this->_tpl_vars['route'][$this->_sections['list']['index']]['code']; ?>
" readonly/>
						
						<?php endif; ?>
					<?php endfor; endif; ?>
				<?php endif; ?>
	</td>
	
	<td> Price :<input type="text" name="price" value="<?php echo $this->_tpl_vars['trip'][0]['cost']; ?>
" required /></td>
	<td id="check" style="display: none;" style=" visibility: hidden;"><input type="checkbox" id="chkbox" name="withS" <?php if ($this->_tpl_vars['trip'][0]['withSubRoute'] == 1): ?> checked<?php endif; ?>> With Sub Routes?</td>
	
	
</tr>
		</table>
		
		
		
		<!--<div class="boxfield">
				<input type="checkbox" id="chkbox" name="withS" <?php if ($this->_tpl_vars['trip'][0]['withSubRoute'] == 1): ?> checked<?php endif; ?>> With Sub Routes?
				</div>-->
		
		 <?php if ($this->_tpl_vars['trip'][0]['withSubRoute'] == 0 || $this->_tpl_vars['op'] == I): ?> 
			<div id="image"  style="display: none;" style=" visibility: hidden;" class="boxfield">
			<?php endif; ?>
		
		<div class="boxfield">
				 <table class="table">
	<thead class="pricing">
	
			<th  width="10%"></th>
			<th  width="50%">Route</th>
			<th  width="10%">Price</th>
			<th  width="10%">Departure Time</th>
	</thead>
<tbody id="pricing">



    <?php echo $this->_tpl_vars['pro_key']; ?>
: <?php echo $this->_tpl_vars['pro']['code']; ?>
 <br />
<?php $this->assign('count', 0); ?>
<?php $_from = $this->_tpl_vars['sroute']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pro_key'] => $this->_tpl_vars['pro']):
?>
<tr>
<td ><?php echo $this->_tpl_vars['count']; ?>
</td>
<td><input type="checkbox" id="chkbox" name="selector[]" value="<?php echo $this->_tpl_vars['count']++; ?>
" <?php if ($this->_tpl_vars['pro']['id'] == $this->_tpl_vars['trip'][0]['code']): ?> checked<?php endif; ?>></td>
	<td>
	<input type="text"  value="<?php echo $this->_tpl_vars['pro']['code']; ?>
" readonly />
				<input type="hidden" name="subroute[]" value="<?php echo $this->_tpl_vars['pro']['id']; ?>
" readonly />
				
	</td>
	
	<td><input type="text" name="cost[]" value="<?php echo $this->_tpl_vars['pro']['price']; ?>
"  /></td>
	<td><input type="text" name="depature[]" value="<?php echo $this->_tpl_vars['pro']['depature']; ?>
"  /></td>
	
	
</tr>

<?php endforeach; endif; unset($_from); ?>

</tbody>

</table>


			
				
			<!-- <input type="button" value="Add" onClick="return newupload()"> -->
			
			 </div>
			  <?php if ($this->_tpl_vars['trip'][0]['withSubRoute'] == 0 || $this->_tpl_vars['op'] == I): ?> 
			</div>
			<?php endif; ?>
        </fieldset>
		
	   <div class="boxfield">
		<input type="submit" value="Confirm" class="serviceSub row_btn"/>
		
		
	    <input type="button" value="Cancel" class="serviceCan row_bt " href="question_list.php" />
		</div>
	</div>
</form>
<script>
<?php echo '
function showUser(str) {
    if (str == "") {
        document.getElementById("pricing").innerHTML = "";
		 document.getElementById("check").style.display = "none";
        return;
    } else {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("pricing").innerHTML = xmlhttp.responseText;
				if(xmlhttp.responseText=="<tr><td colspan=\'3\'>No sub routes on this route</td></tr>")
				document.getElementById("check").style.display = "none";
				else
				document.getElementById("check").style.display = "block";
            }
        }
        xmlhttp.open("GET","getroutes.php?q="+str,true);
        xmlhttp.send();
    }
}
'; ?>

</script>