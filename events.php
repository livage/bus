<?php

require('inc.check.php');
require_once('core/func.mysqlPrepare.php');
require_once('core/func.getListOptions.php');
require_once('core/func.getSqlOptions.php');



$actPage = ($_GET['pg']?intval($_GET['pg']):1);
$id = intval($_GET['id']);
$select = ($_GET['list']);

switch ($_TREART['operation']) {

  case 'P': // Insert
       		$sql = 'SELECT	COUNT(*) counter
					FROM	matches 			
					WHERE	1  AND match_date >= CURDATE()';
					
			$data = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			
			if ($results = $data['counter']) {
				require('inc.pagination.php');
				$sql = 'SELECT	m.*,v.name vname,t1.name t1name,t2.name t2name
						FROM	matches m 
						INNER JOIN venue v ON m.venue = v.id
						INNER JOIN team t1  ON m.team1 = t1.id
						INNER JOIN team t2 ON m.team2 = t2.id 						
						WHERE	1 AND match_date >= CURDATE()
						ORDER BY match_date 
						LIMIT	'.(($actPage-1)*RESULT_PER_PAGE).", ".RESULT_PER_PAGE;
				
				$match = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
				}
				
     
       
        break;

    case 'I': // Insert
		
      $sql =	'SELECT	*
				FROM	team
				where active=1';
      $team= sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
	  
	  $sql =	'SELECT	*
				FROM	venue
				where active=1';
      $venue= sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
        
        break;
    /*********************************************************************************************************/
    case 'U': // Update   $opString = 'Modifica';
	 $sql =	'SELECT	*
				FROM	team
				where active=1';
      $team= sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
	  
	  $sql =	'SELECT	*
				FROM	venue
				where active=1';
      $venue= sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
	  
        $sql1 =	'SELECT	*,DATE_FORMAT(match_date,"%d/%m/%Y") AS mdate
				FROM	matches
				where id='.$id;
        
        $match= sqlExecute($connection, $sql1, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
		
		 $sql1 =	'SELECT	*
				FROM	seat_allocation
				where match_id='.$id;
        
        $seats= sqlExecute($connection, $sql1, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
		
        break;
    
    /************************************************************************************************************************/
    default: // List
       		$sql = 'SELECT	COUNT(*) counter
					FROM	event			
					WHERE	1 ';
					
			$data = sqlExecute($connection, array('sqlStatement'=>$sql, 'rowLimit'=>1), $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			
			if ($results = $data['counter']) {
				require('inc.pagination.php');
				$sql = 'SELECT	m.*,DATE_FORMAT(edate,"%d %b %Y") AS mdate ,v.name ,(SELECT SUM(`allocation`) total_seats FROM `seat_allocation` sa WHERE `match_id`= m.activityID) allocatedtickets,
		(SELECT COUNT(`id`) FROM `transactions` t WHERE `match_id`= m.activityID) seats_sold
						FROM	event m 
						INNER JOIN venue v ON m.venue = v.id
						WHERE	1 
						ORDER BY edate 
						LIMIT	'.(($actPage-1)*RESULT_PER_PAGE).", ".RESULT_PER_PAGE;
				//echo $sql;
				$match = sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
				}
				
     
       
        break;
    
}

$tmplEngine->assign(
        array(
        'filter'        		=>  $_SESSION[SITE_NAME]['usersFilter'],
        'match'   			=>  $match,
		'venue'   			=>  $venue,
		'team'   			=>  $team,
        'seats'   			=>  $seats,
        )
);
?>