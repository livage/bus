<?php
require('inc.init.php');
require('func.microtimeFloat.php');
$timeElapsed = microtimeFloat();
$sqlCount = 0;

require_once('core/inc.config.php');

$_TREART = array_merge(
		is_array($_TREART)?$_TREART:array(),
		array('content' => $_GET['c']?$_GET['c']:DEFAULT_PAGE)
);

if (DEBUG) {
    require_once('func.writeLog.php');
    writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], " ---=== <span class='page_start'>PAGE GENERATION START</span> ===---", "", $_TREART, basename(__FILE__),'E',0);
}

// Init Smarty
require('core/class.tmpl_engine.php');
$tmplEngine = new T_tmpl_engine();

// Try lo load content
if ($_TREART['content']) {
    $sql =  ' 	SELECT  t.template_page, t.master_title, t.keywords AS master_keywords, t.php_page AS master_php_page,
    					c.title, c.keywords, c.content_page, c.php_page, c.operation, c.content_class
            	FROM  	_contents AS c
            	LEFT JOIN
            			_templates AS t
        			ON  t.template_id = c.template_id
				WHERE   c.content_name = \''.mysql_real_escape_string($_TREART['content']).'\'
				AND     ( t.access_type LIKE \'%'.$_SESSION[SITE_NAME]['login_data']['profile'].'%\'
						OR  t.template_id IS NULL )
				AND     c.access_type LIKE \'%'.$_SESSION[SITE_NAME]['login_data']['profile'].'%\'
				AND     (t.active = 1 OR t.template_id IS NULL)
				AND     c.active = 1';
//  echo $sql;
    $contentData = sqlExecute( $connection,
                                array(  'rowLimit'      => 1,
                                        'sqlStatement'  => $sql ),
                                $sqlError,
                                $sqlCount,
                                basename(__FILE__),
                                DEBUG);
    // If no content found and not default content
    if (!$contentData) {
    	writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking content", "Force to default page " . DEFAULT_PAGE, $_TREART, basename(__FILE__),'E',0);
        if ($_TREART["content"] != DEFAULT_PAGE) {
            $sql =  " SELECT  t.template_page, t.master_title, t.keywords AS master_keywords, t.php_page AS master_php_page, ".
                    "         c.title, c.keywords, c.content_page, c.php_page, c.operation , c.content_class ".
          		    "	FROM  _contents AS c ".
          		    "   LEFT JOIN  ".
          		    "         _templates AS t ".
          		    "     ON  t.template_id = c.template_id ".
                    " WHERE   c.content_name = '".mysql_escape_string(DEFAULT_PAGE)."' ".
                    " AND     ( t.access_type LIKE '%".($_SESSION[SITE_NAME]["login_data"]["profile"]?$_SESSION[SITE_NAME]["login_data"]["profile"]:"O")."%'".
                    "         OR  t.template_id IS NULL ) ".
                    " AND     c.access_type LIKE '%".($_SESSION[SITE_NAME]["login_data"]["profile"]?$_SESSION[SITE_NAME]["login_data"]["profile"]:"O")."%'".
                    " AND     (t.active = 1 OR t.template_id IS NULL)".
                    " AND     c.active = 1";
            // If no data found
            $contentData = sqlExecute(  $connection,
                                        array(  "rowLimit"      => 1,
                                                "sqlStatement"  => $sql ),
                                        $sqlError,
                                        $sqlCount,
                                        basename(__FILE__),
                                        DEBUG);
            require_once("func.writeLog.php");
            if (!$contentData)
                writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "No content specified", "Missing Content" . $_TREART["content"], $_TREART, basename(__FILE__),'E',1);
            else
                writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking content", "Content " .DEFAULT_PAGE . " found", $_TREART, basename(__FILE__),'E',0);
        }
    } else {
		writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking content", "Content " . $_TREART["content"] . " found", $_TREART, basename(__FILE__),'E',0);
	}
} else {
	require_once("func.writeLog.php");
    writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "No content specified", "No content specified", $_TREART, basename(__FILE__),'E',1);
}

if($contentData){
	$_TREART = array_merge(
		is_array($_TREART)?$_TREART:array(),
		array(
			// Template
			'templateHTML' 		=> $contentData['template_page'],
			'templateTitle' 	=> $contentData['master_title'],
			'templateKeywords' 	=> $contentData['master_keywords'],
			'templatePHP' 		=> $contentData['master_php_page'],
			// Content
			'contentHTML' 		=> $contentData['content_page'],
			'contentTitle' 		=> $contentData['title'],
			'contentKeywords' 	=> $contentData['keywords'],
			'contentPHP' 		=> $contentData['php_page'],
			'operation' 		=> $contentData['operation'],
			'contentClass' 		=> $contentData['content_class'],
			// Other
			'keywords' 			=> "$templateKeywords, $contentKeywords",
//			'title' 			=> $templateTitle.$contentTitle,
	   )
	);
	if ($_TREART['templatePHP'] && file_exists($_TREART['templatePHP']))
		require_once($_TREART['templatePHP']);
	elseif ($_TREART['templatePHP']) {
		require_once('func.writeLog.php');
		writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking PHP Template file '".$_TREART["templatePHP"]."'", "Missing file", $_TREART, basename(__FILE__),'E',0);
	}

	if ($_TREART['contentPHP'] && file_exists($_TREART['contentPHP']))
		require_once($_TREART['contentPHP']);
	elseif ($_TREART['contentPHP']) {
		require_once('func.writeLog.php');
		writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking PHP Content file '".$_TREART["contentPHP"]."'", "Missing file", $_TREART, basename(__FILE__),'E',0);
	}
	if ($_TREART['contentHTML'] && file_exists('htmls/'.$_TREART['contentHTML']))
		$tmplEngine->assign('page_content', $_TREART['contentHTML']);
	elseif ($_TREART['contentHTML']) {
		require_once('func.writeLog.php');
		writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking HTML Content file '".$_TREART["contentHTML"]."'", "Missing file", $_TREART, basename(__FILE__),'E',0);
	}
}

//$timeElapsed = microtimeFloat()-$timeElapsed;
$timeElapsed = 0;
$tmplEngine->assign(
    array(  'userdata'          => $_SESSION[SITE_NAME]['login_data'],
            'loginMessage'   	=> $_SESSION[SITE_NAME]['login_error'],
            '_ENGINE'			=> $_TREART,
            'sqlExecuted'    	=> $sqlCount,
            'generationTime' 	=> $timeElapsed,
            'content_class' 	=> $timeElapsed,
//			'_language'			=> $_SESSION[SITE_NAME]['language'],
    )
);
$_SESSION[SITE_NAME]["login_error"] = "";

require('core/inc.translate.php');

if ($_TREART['templateHTML'] && file_exists('htmls/template/'.$_TREART['templateHTML']))
    $tmplEngine->display('template/'.$_TREART['templateHTML']);
elseif ($_TREART['templateHTML']) {
    require_once('func.writeLog.php');
    writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], "Looking HTML Template file '". $_TREART["templateHTML"]. "'", "Missing file", $_TREART, basename(__FILE__),'E',0);
}

if (DEBUG) {
    require_once('func.writeLog.php');
    writeLog($connection, basename($_SERVER['PHP_SELF']), $_SERVER['QUERY_STRING'], " ---=== <span class='page_end'>PAGE GENERATION END</span> ===---", "Generation Time: $timeElapsed sec| Queries: $sqlCount", $_TREART, basename(__FILE__),'E',0);
}

//echo '**'.$_SESSION[SITE_NAME]['language'].'**';
//print_r($_SESSION[SITE_NAME]);
?>