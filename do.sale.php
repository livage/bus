<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');
require_once('encoder.php');

if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_POST['id']);
}


$fields = array(
	'STRING' => array(
		'section',
			'paymentref',				
	),
	'INT' => array(
	'match_id',
	
	),
	'FLOAT' => array(
	'amount',
	),
	'DATE' => array(
	
	),
	'DATETIME' => array(
	),
);

$mainTable = 'transactions';
$nextPage = 'index.php?c=sale&id='.$_POST['match_id'];

$input=array('section'=>$_POST['section'][$id-1],
'amount'=>$_POST['amount'][$id-1],
'match_id'=>$_POST['match_id'],
'paymentref'=>"Cash",
);

switch ($op) {
    
    case 'I': // Insert
    
      $transid = doOperation($connection, 'I', $mainTable, $fields, $input, $sqlError);
	 // $content =file_get_contents("ticket.php?match=".$_POST['match_id']."&amount=".$_POST['amount'][$id-1]."&section=".$_POST['section'][$id-1]);
	  $ticket="ticket.php?match=".$_POST['match_id']."&amount=".$_POST['amount'][$id-1]."&section=".$_POST['section'][$id-1]."&ticket=".encoder ($transid);
	 // echo $ticket;
	 ?> 
	 <script language="javascript" type="text/javascript">
   window.location = "<?php echo $ticket;?>";
</script>
<?php
	  ///printer setting 
		$handle = fopen("PRN", "w");
	  
	 
		if (!$handle){
		die('no connection');
		}

		$data = " PRINT THIS ";

		// Cut Paper
		$data .= "\x00\x1Bi\x00";

		if (!fwrite($handle,$data)){
			die('writing failed');
		}
	
	  fclose($handle);

        break;
    case "U": // Update
    	
        doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
		
        break;
      case 'D'://Status
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
       
   case 'S'://Status
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
    
}
?>
<script language="javascript" type="text/javascript">
   window.location = "<?php echo $nextPage?>";
</script>