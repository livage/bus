<?php
require('inc.init.php');
require('core/inc.config.php');
require_once('core/func.nvl.php');
require_once('core/func.doOperation.php');
require_once('func.storeImage.php');
require_once('core/func.mysqlPrepare.php');


if(!$op) {
	$op = strtoupper($_GET['op']?$_GET['op']:$_POST['op']);
	$id = intval($_GET['id']);
}

$fields = array(
	'STRING' => array(
		'code',
		'description',
	),
	'INT' => array(
	'stationA',
	'stationB',
	'duration',
	
	),
	'FLOAT' => array(
	'cost',
	),
	'DATE' => array(
	
	),
	'DATETIME' => array(
	
	),
	'TIME' => array(
	
	),
);

$mainTable = 'routes';
$nextPage = 'index.php?c=route_list';

switch ($op) {
    
    case 'I': // Insert
    


       $id = doOperation($connection, 'I',$mainTable, $fields, $_POST, $sqlError); 
	   
	   
	    if($_POST['has_subroutes']=='on'){ 
		
			$sql = "UPDATE	$mainTable	SET has_subroutes=1	WHERE 	id = $id";
			sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			
			
			foreach ($_POST['route'] as $value){
				
				$sql = "insert into	route_subroutes (route,subroute,company,insU,insTS) VALUES ( $id,$value,'".$_SESSION[SITE_NAME]['login_data']['company']."','".$_SESSION[SITE_NAME]['login_data']['username']."',NOW() ) ";
				
			sqlExecute($connection, $sql, $sqlError, $sqlCount, basename(__FILE__), DEBUG);
			}
			
		//exit;
		}
	
        break;
		
		
    case "U": // Update
    	
        doOperation($connection, 'U', $mainTable, $fields, $_POST, $sqlError, $id);
	

        break;
		
      case 'D'://Status
			doOperation($connection, 'D', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
       
   case 'S'://Status
			doOperation($connection, 'S', $mainTable, $fields, $_POST, $sqlError, $id);
        break;
    
}
?>
<script language="javascript" type="text/javascript">
   window.location = "<?php echo $nextPage?>";
</script>